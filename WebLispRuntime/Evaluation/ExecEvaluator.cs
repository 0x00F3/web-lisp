﻿using ApiModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Evaluation
{
    public class ExecEvaluator
    {
        public Result<LiteralModel> Execute(ExecutionModel model)
            => model.Dfn.Type.Name == "boolean"
            ? new ExpEvaluator().Boolean(model.Dfn.Body, model.RunArgs)
                .Map(b => new LiteralModel(b.ToString()))
            : model.Dfn.Type.Name == "date"
            ? throw new NotImplementedException()
            : model.Dfn.Type.Name == "list"
            ? model.Dfn.Type.MaybeT()
                .toResult("Type \"list\" requires a generic type parameter. ")
                .FlatMap(t => ExpEvaluator.List(model.Dfn.Body, model.RunArgs, t))
            : model.Dfn.Type.Name == "number"
            ? new ExpEvaluator().Number(model.Dfn.Body, model.RunArgs)
                .Map(dec => new LiteralModel(dec.ToString()))
            : model.Dfn.Type.Name == "string"
            ? new ExpEvaluator().String(model.Dfn.Body, model.RunArgs)
                .Map(str => new LiteralModel(str))
            : model.Dfn.Type.Name == "tuple"
            ? new ExpEvaluator().Tuple(model.Dfn.Body, model.RunArgs, model.Dfn.Type.Generics)
            : new Result<LiteralModel>(
                ImmutableList.Create($"Invalid definition type {model.Dfn.Type.Name}. ")
                )
            ;

    }
}
