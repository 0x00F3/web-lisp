﻿using ApiModel;
using Core;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Evaluation
{
    public class LambdaModel
    {
        public DefinitionModel Dfn { get; }
        public IImmutableList<string> parameterNames { get; }
        
        public LambdaModel(DefinitionModel dfn, 
            IImmutableList<ParameterModel> typeParameters)
        {
            Dfn = dfn;
            parameterNames = typeParameters
                .Where(t => t.Name != "TRETURN")
                .Select(t => t.Name)
                .ToImmutableList();
        }

        public Result<ExecutionModel> ToExecutable(params LiteralModel[] runArgs)
        {
            try
            {
                IImmutableList<RunArgModel> result = ImmutableList
                    .Create<RunArgModel>();
                int k = parameterNames.Count();
                for (int i = 0; i < k; i++)
                {
                    result = result.Add(new RunArgModel
                    {
                        Name = parameterNames.ElementAt(i),
                        Literal = runArgs.ElementAt(i)
                    });
                }
                return new Result<ExecutionModel>(new ExecutionModel
                {
                    Dfn = Dfn,
                    RunArgs = result
                });
            }
            catch
            {
                return new Maybe<ExecutionModel>()
                    .toResult("Failed to create executable lambda based on given parameters.");
            }
            
        }


    }
}
