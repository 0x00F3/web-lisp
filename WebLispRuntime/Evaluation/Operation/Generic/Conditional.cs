﻿using ApiModel;
using Core;
using Evaluation.Operation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Evaluation.Operation.Generic
{
    internal class Conditional : AbstractOperationFactory
    {
        public Result<LiteralModel> Evaluate(IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            , TypeModel t)
            => condition(args, context)
                .FlatMap(condition => condition ? pass(args, context, t) : fail(args, context, t))
            ;

        /*----------------------------parameters-----------------------------*/
        private Result<bool> condition(
            IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context)
            => FindBoolean("condition", "if", args, context)
            ;

        private Result<LiteralModel> pass(IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            , TypeModel t)
            => args.Where(arg => arg.Name == "pass")
                .MaybeFirst()
                .Map(arg => new ExecutionModel
                {
                    Dfn = new DefinitionModel
                    {
                        Type = t,
                        Body = arg.Expression
                    },
                    RunArgs = context
                })
                .toResult("No argument found for \"if\" called \"pass\". ")
                .FlatMap(executable => new ExecEvaluator().Execute(executable))
            ;

        private Result<LiteralModel> fail(IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            , TypeModel t)
            => args.Where(arg => arg.Name == "fail")
                .MaybeFirst()
                .Map(arg => new ExecutionModel
                {
                    Dfn = new DefinitionModel
                    {
                        Type = t,
                        Body = arg.Expression
                    },
                    RunArgs = context
                })
                .toResult("No argument found for \"if\" called \"fail\". ")
                .FlatMap(executable => new ExecEvaluator().Execute(executable))
                //.Match(literal => new Result<LiteralModel>(literal), errors => throw new Exception(JsonConvert.SerializeObject(args)))
            ;

        
    }
}
