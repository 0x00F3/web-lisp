﻿using ApiModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Evaluation.Operation.Generic
{
    class Application : AbstractOperationFactory
    {
        internal Result<LiteralModel> Evaluate(
            ExpressionModel expr
            , IImmutableList<RunArgModel> context
            , TypeModel t
            )
            => operand(expr.Arguments, tprev(expr.Types), context)
            .Map(op => (op, FindFunction("f", "apply", expr.Arguments, context, t)))
            //unwrap
            .FlatMap(tuple => tuple.Item2.Map(func => (tuple.Item1, func)))
            .FlatMap(tuple => tuple.Item2.ToExecutable(tuple.Item1.Values.ToArray()))
            .FlatMap(executable => new ExecEvaluator().Execute(executable));

        /* inputs */
        private TypeModel tprev(IImmutableList<ParameterModel> types)
            => new TypeModel
            {
                Name = "tuple",
                Generics = types.Select(p => p.Type).ToImmutableList()
            };

        private Result<LiteralModel> operand(IImmutableList<ArgumentModel> args,
            TypeModel type,
            IImmutableList<RunArgModel> context)
            => Find("operand", "apply", args, type, context);




    }
}
