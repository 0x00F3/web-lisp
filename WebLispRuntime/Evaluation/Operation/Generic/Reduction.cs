﻿using ApiModel;
using Core;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Evaluation.Operation.Generic
{
    internal class Reduction : AbstractOperationFactory
    {
        internal Result<LiteralModel> Evaluate(
           ExpressionModel expr
           , IImmutableList<RunArgModel> context
           , TypeModel tnext
           )
            => tprev(expr)
            .Map(tprev => (operands(expr.Arguments, tprev, context),
                           initial(expr.Arguments, tprev, context),
                           FindFunction("f", "map", expr.Arguments, context, tnext)
                          )
            )
            /* unwrap first and last. Item2 shoud stay wrapped in a result in
             * conformity with aggregate() */
            .FlatMap(tuple => tuple.Item1.Map(operand => (operand, tuple.Item2, tuple.Item3)))
            .FlatMap(tuple => tuple.Item3.Map(func => (tuple.Item1, tuple.Item2, func)))
            
            // aggregate and execute
            .FlatMap(tup => aggregate(tup.Item1.Values, tup.Item2, tup.Item3, context))
            ;

        /* inputs */
        private Result<LiteralModel> initial(IImmutableList<ArgumentModel> args,
            TypeModel tprev,
            IImmutableList<RunArgModel> context
            )
            => args.Where(arg => arg.Name == "initial")
            .MaybeFirst()
            .toResult("Operation 'reduce' requires argument 'initial'. ")
            .Map(arg => arg.Expression)
            .Map(expr => new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Body = expr,
                    Type = tprev
                },
                RunArgs = context
            })
            .FlatMap(executable => new ExecEvaluator().Execute(executable))
            ;

        private Result<LiteralModel> operands(IImmutableList<ArgumentModel> args,
            TypeModel tprev,
            IImmutableList<RunArgModel> context
            )
            => args.Where(arg => arg.Name == "operand")
            .MaybeFirst()
            .toResult("Operation 'reduce' requires argument 'operand'. ")
            .Map(arg => arg.Expression)
            .Map(expr => new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Body = expr,
                    Type = new TypeModel("list", tprev)
                },
                RunArgs = context
            })
            .FlatMap(executable => new ExecEvaluator().Execute(executable))
            ;

        private Result<TypeModel> tprev(ExpressionModel expr)
            => expr.Types.Where(typeParameter => typeParameter.Name == "T")
            .MaybeFirst()
            .Map(typeParameter => typeParameter.Type)
            .toResult("Operation 'reduce' requres type parameter T. ");

        /* helpers */
        private Result<LiteralModel> aggregate(
            IImmutableList<LiteralModel> literals,
            Result<LiteralModel> initial,
            LambdaModel f,
            IImmutableList<RunArgModel> context
            )
            => literals.Aggregate(initial,
                (accumulator, element) => accumulator
                    .FlatMap(acc => f.ToExecutable(acc, element)
                        .Map(exec => new ExecutionModel
                        {
                            Dfn = exec.Dfn,
                            RunArgs = context.AddRange(exec.RunArgs)
                        })
                        .FlatMap(exec => new ExecEvaluator().Execute(exec))
                    )
                );

    }
}
