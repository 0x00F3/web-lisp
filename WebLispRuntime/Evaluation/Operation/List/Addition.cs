﻿using ApiModel;
using Core;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Evaluation.Operation.List
{
    internal class Addition
    {
        internal Result<LiteralModel> Evaluate(IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            , TypeModel type
            )
            => list(args, context, type)
                .Match(els => element(args, type, context)
                    .FlatMap(el => evaluateHelper(els, el))
                , errors => new Result<LiteralModel>(element(args, type, context).Errors().AddRange(errors))
                ); 

        /*---------------------------parameters-----------------------*/
        private Result<IImmutableList<LiteralModel>> list(
            IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            , TypeModel tnext)
            => args.Where(arg => arg.Name == "list")
                .MaybeFirst()
                .toResult("No argument found for \"append\" called \"list\". ")
                .FlatMap(arg => ExpEvaluator.List(arg.Expression, context, new TypeModel("list", tnext)))
                .Map(literal => literal.Values)
            ;

        private Result<LiteralModel> element(
            IImmutableList<ArgumentModel> args
            , TypeModel t
            , IImmutableList<RunArgModel> context
            )
            => args.Where(arg => arg.Name == "element")
                .MaybeFirst()
                .toResult("No argument found for \"append\" called \"element\". ")
                .Map(arg => new ExecutionModel
                {
                    Dfn = new DefinitionModel
                    {
                        Type = t,
                        Body = arg.Expression
                    },
                    RunArgs = context
                })
                .FlatMap(executable => new ExecEvaluator().Execute(executable));

        /*-------------------helpers-------------*/
        private Result<LiteralModel> evaluateHelper(IImmutableList<LiteralModel> literals
            , LiteralModel element
            )
            => new Result<LiteralModel>(new LiteralModel(literals.Add(element)));

        //internal Result<LiteralModel> Create(IImmutableList<ArgumentModel> args
        //    , TypeModel t
        //    , IImmutableList<RunArgModel> context)
        //    => list(args, context)
        //        .Match(es => element(args, t, context)
        //        , errors => new Result<LiteralModel>(element(args, t, context).AddErrors(errors).Errors())
        //        );

        ///*-------------------------get parameters----------------------------*/
        //private Result<IImmutableList<ValueModel>> list(
        //    IImmutableList<ArgumentModel> args
        //    , IImmutableList<RunArgModel> context
        //    )
        //    => listExpression(args, context)
        //    .FlatMap(model => invert(model, context));


        //private Result<LiteralModel> element(
        //    IImmutableList<ArgumentModel> args
        //    , TypeModel t
        //    , IImmutableList<RunArgModel> context
        //    )
        //    => args.Where(arg => arg.Name == "element")
        //        .MaybeFirst()
        //        .toResult("No argument found for \"append\" called \"element\". ")
        //        .Map(arg => new ExecutionModel
        //        {
        //            Dfn = new DefinitionModel
        //            {
        //                Type = t,
        //                Body = arg.Expression
        //            },
        //            RunArgs = context
        //        })
        //        .FlatMap(executable => new ExecEvaluator().Execute(executable));

        ///*---------------------------helpers---------------------------------*/

        ///// <summary>
        ///// converts an Expression of a List to a List of an Expression.
        ///// </summary>
        ///// <param name="model"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //private Result<IImmutableList<ValueModel>> invert(
        //    ExpressionModel model
        //    , IImmutableList<RunArgModel> context)
        //    => model.Either()
        //    .Match(args => operandError()
        //    , value => resolveAsList(value, context)
        //    );

        //private Result<ExpressionModel> listExpression(
        //    IImmutableList<ArgumentModel> args
        //    , IImmutableList<RunArgModel> context
        //    )
        //    => args.Where(m => m.Name == "list")
        //        .MaybeFirst()
        //        .toResult("No argument found for \"append\" called \"list\". ")
        //        .Map(arg => arg.Expression);

        //private Result<IImmutableList<LiteralModel>> resolveAsList(LiteralModel value
        //    , IImmutableList<RunArgModel> context)
        //    => value.Either()
        //    .Match(str => resolveFromContext(str, context)
        //    , values => new Result<IImmutableList<LiteralModel>>(values)
        //    );


        //private Result<IImmutableList<LiteralModel>> resolveFromContext(string name
        //    , IImmutableList<RunArgModel> context)
        //    => context.Where(arg => arg.Name == name)
        //        .MaybeFirst()
        //        .Map(arg => arg.Values)
        //        .toResult($"Could not resolve {name} as list of values. ");

        //private Result<IImmutableList<ValueModel>> operandError()
        //    => new Result<IImmutableList<ValueModel>>(
        //        ImmutableList.Create("Operand for map not a list of values. ")
        //    );
    }
}
