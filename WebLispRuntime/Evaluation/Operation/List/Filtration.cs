﻿using ApiModel;
using Core;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Evaluation.Operation.List
{

    internal class Filtration : AbstractOperationFactory
    {
        internal Result<LiteralModel> Evaluate(
            ExpressionModel expr
            , IImmutableList<RunArgModel> context
            , TypeModel type
            )
            => FindFunction("f", "filter", expr.Arguments, context, new TypeModel("boolean"))
            .Map(f => (f, FindList("operand", "filter", expr.Arguments, context, type)))
            // unwrap
            .FlatMap(pair => pair.Item2.Map(list => (pair.Item1, list)))
            .Map(pair => pair.Item2.Values.Select(x => (x, filterCondition(x, pair.Item1))))
            .Map(pairs => pairs.Select(pair => pair.Item2.Map(boolean => (pair.Item1, boolean))))
            .FlatMap(results => results.Invert())
            .Map(pairs => pairs.Where(pair => pair.Item2))
            .Map(pairs => pairs.Select(pair => pair.Item1))
            .Map(list => new LiteralModel("list", list.ToImmutableList()));


        /* helpers */
        private Result<bool> filterCondition(LiteralModel ltrl, LambdaModel lambda)
            => lambda.ToExecutable(ltrl)
            .FlatMap(executable => new ExecEvaluator().Execute(executable))
            .FlatMap(result => LiteralEvaluator.Boolean(result.Value));
    }
}
