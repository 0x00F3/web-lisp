﻿using ApiModel;
using Core;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Evaluation.Operation.List
{
    internal class Head
    {
        internal Result<LiteralModel> Evaluate(
            IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            , TypeModel t
            )
            => operand(args, context, t)
                .FlatMap(literals =>
                    literals.MaybeFirst()
                    .toResult("Cannot get head of empty list.")
                );

        /*---------------------------parameters------------------------------*/

        private Result<IImmutableList<LiteralModel>> operand(
            IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            , TypeModel t)
            => args.Where(arg => arg.Name == "operand")
                .MaybeFirst()
                .toResult("No argument found for \"map\" called \"operand\". ")
                .FlatMap(arg => ExpEvaluator.List(arg.Expression, context, new TypeModel("list", t)))
                .Map(literal => literal.Values)
            ;
    }
}
