﻿using ApiModel;
using Core;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Evaluation.Operation.List
{
    internal class Projection : AbstractOperationFactory
    {
        internal Result<LiteralModel> Evaluate(ExpressionModel expr,
            IImmutableList<RunArgModel> context,
            TypeModel tnext
            )
            => tprev(expr)
            .Map(t => (operand(expr.Arguments, t, context), f(expr.Arguments, context, tnext)))
            // unwrap
            .FlatMap(pair => pair.Item1.FlatMap(operand => pair.Item2.Map(f => (operand, f))))
            .Map(pair => pair.Item1.Values.Select(x => pair.Item2.ToExecutable(x)))
            .FlatMap(results => results.Invert())
            .Map(executables => executables.Select(executable => new ExecutionModel
            {
                Dfn = executable.Dfn,
                RunArgs = executable.RunArgs.AddRange(context)
            }))
            .Map(executables => executables
                .Select(executable => new ExecEvaluator().Execute(executable)))
            .FlatMap(results => results.Invert())
            .Map(values => new LiteralModel
            {
                Value = "list",
                Values = values
            });
            
            


        /*---------------------------parameters------------------------------*/
        private Result<LiteralModel> operand(
            IImmutableList<ArgumentModel> args,
            TypeModel t,
            IImmutableList<RunArgModel> context
            )
            => FindList("operand", "map", args, context, t);

        private Result<LambdaModel> f(
            IImmutableList<ArgumentModel> args,
            IImmutableList<RunArgModel> context,
            TypeModel outputType
            )
            => FindFunction("f", "map", args, context, outputType);

        private Result<TypeModel> tprev(ExpressionModel expr)
            => expr.Types.Where(typeParameter => typeParameter.Name == "T")
            .MaybeFirst()
            .Map(typeParameter => typeParameter.Type)
            .toResult("Operation 'map' requres type parameter T. ");

        /*--------------------------------helpers----------------------------*/

    }
}
