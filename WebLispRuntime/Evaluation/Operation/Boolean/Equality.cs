﻿using ApiModel;
using System.Collections.Immutable;
using System.Text;

namespace Evaluation.Operation.Boolean
{
    internal class Equality : AbstractOperationFactory
    {
        internal Result<bool> Evaluate(
            IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            )
            => first(args, context)
                .Match(f => second(args, context)
                    .Map(s => f == s)
                , errors => new Result<bool>(second(args, context).Errors().AddRange(errors)
                    )
                );

        /*----------------------------get parameters-------------------------*/
        private Result<decimal> first(
            IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            )
            => FindNumber("first", "equal", args, context);

        private Result<decimal> second(
            IImmutableList<ArgumentModel> args
           , IImmutableList<RunArgModel> context
           )
           => FindNumber("second", "equal", args, context);
    }
}
