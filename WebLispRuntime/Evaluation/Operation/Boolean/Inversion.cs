﻿using ApiModel;
using System.Collections.Immutable;
using System.Text;

namespace Evaluation.Operation.Boolean
{
    internal class Inversion : AbstractOperationFactory
    {
        internal Result<bool> Evaluate(IImmutableList<ArgumentModel> models
            , IImmutableList<RunArgModel> context
            )
            => operand(models, context)
                .Map(op => ! op);

        /*----------------------------get parameters-------------------------*/
        private Result<bool> operand(IImmutableList<ArgumentModel> models
            , IImmutableList<RunArgModel> context
            )
            => FindBoolean("operand", "not", models, context);
    }
}
