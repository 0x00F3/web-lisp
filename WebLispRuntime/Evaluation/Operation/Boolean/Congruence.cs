﻿using ApiModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Evaluation.Operation.Boolean
{
    internal class Congruence : AbstractOperationFactory
    {
        internal Result<bool> Evaluate(
            IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            )
            => first(args, context)
                .Match(f => second(args, context)
                    .Map(s => f == s)
                , errors => new Result<bool>(second(args, context).Errors().AddRange(errors)
                    )
                );

        /*----------------------------get parameters-------------------------*/
        private Result<string> first(
            IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            )
            => FindString("first", "congruent", args, context);

        private Result<string> second(
            IImmutableList<ArgumentModel> args
           , IImmutableList<RunArgModel> context
           )
           => FindString("second", "congruent", args, context);
    }
}
