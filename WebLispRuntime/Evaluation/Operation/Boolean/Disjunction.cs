﻿using ApiModel;
using System.Collections.Immutable;
using System.Text;

namespace Evaluation.Operation.Boolean
{
    /// <summary>
    /// logical "or" operator
    /// </summary>
    internal class Disjunction : AbstractOperationFactory
    {
        internal Result<bool> Evaluate(
            IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            )
            => first(args, context)
                .Match(f => second(args, context)
                    .Map(s => f | s)
                , errors => new Result<bool>(second(args, context).Errors().AddRange(errors)
                    )
                );

        /*----------------------------get parameters-------------------------*/
        private Result<bool> first(
            IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            )
            => FindBoolean("first", "or", args, context);

        private Result<bool> second(
            IImmutableList<ArgumentModel> args
           , IImmutableList<RunArgModel> context
           )
           => FindBoolean("second", "or", args, context);
    }
}
