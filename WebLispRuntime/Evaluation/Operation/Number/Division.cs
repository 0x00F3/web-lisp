﻿using ApiModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Evaluation.Operation.Number
{
    internal class Division : AbstractOperationFactory
    {
        internal Result<decimal> Evaluate(
            IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            )
            => dividend(args, context)
                .Match(idend => divisor(args, context)
                    .Map(isor => idend / isor)
                , errors => new Result<decimal>(divisor(args, context)
                    .Errors().AddRange(errors)
                    )
                );

        /*----------------------------get parameters-------------------------*/

        private Result<decimal> dividend(
            IImmutableList<ArgumentModel> models
            , IImmutableList<RunArgModel> context
            )
            => FindNumber("dividend", "divide", models, context);

        private Result<decimal> divisor(
            IImmutableList<ArgumentModel> models
            , IImmutableList<RunArgModel> context
            )
            => FindNumber("divisor", "divide", models, context);
    }
}
