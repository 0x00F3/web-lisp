﻿using ApiModel;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Evaluation.Operation.Number
{
    internal class Addition : AbstractOperationFactory
    {
        internal Result<decimal> Evaluate(
            IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            )
            => augend(args, context)
                .Match(aug => addend(args, context)
                    .Map<decimal>(add => aug + add)
                , errors => new Result<decimal>(addend(args, context)
                    .Errors().AddRange(errors)
                    )
                );


        /*----------------------------get parameters-------------------------*/

        private Result<decimal> addend(
            IImmutableList<ArgumentModel> models
            , IImmutableList<RunArgModel> context
            )
            => FindNumber("addend", "add", models, context);

        private Result<decimal> augend(
            IImmutableList<ArgumentModel> models
            , IImmutableList<RunArgModel> context
            )
            => FindNumber("augend", "add", models, context);
    }
}
