﻿using ApiModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Evaluation.Operation.Number
{
    internal class Subtraction : AbstractOperationFactory
    {
        internal Result<decimal> Evaluate(
            IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            )
            => minuend(args, context)
                .Match(min => subtrahend(args, context)
                    .Map(sub => min - sub)
                , errors => new Result<decimal>(subtrahend(args, context)
                    .Errors().AddRange(errors)
                    )
                );

        /*----------------------------get parameters-------------------------*/

        private Result<decimal> minuend(
            IImmutableList<ArgumentModel> models
            , IImmutableList<RunArgModel> context
            )
            => FindNumber("minuend", "subtract", models, context);

        private Result<decimal> subtrahend(
            IImmutableList<ArgumentModel> models
            , IImmutableList<RunArgModel> context
            )
            => FindNumber("subtrahend", "subtract", models, context);
    }
}
