﻿using ApiModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Evaluation.Operation.Number
{
    internal class Multiplication : AbstractOperationFactory
    {
        internal Result<decimal> Evaluate(
            IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            )
            => multiplicand(args, context)
                .Match(and => multiplier(args, context)
                    .Map(ier => and * ier)
                , errors => new Result<decimal>(multiplier(args, context)
                    .Errors().AddRange(errors)
                    )
                );

        /*----------------------------get parameters-------------------------*/

        private Result<decimal> multiplicand(
            IImmutableList<ArgumentModel> models
            , IImmutableList<RunArgModel> context
            )
            => FindNumber("multiplicand", "multiply", models, context);

        private Result<decimal> multiplier(
            IImmutableList<ArgumentModel> models
            , IImmutableList<RunArgModel> context
            )
            => FindNumber("multiplier", "multiply", models, context);

    }
}
