﻿using ApiModel;
using Core;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Evaluation.Operation
{
    internal abstract class AbstractOperationFactory
    {
        protected Result<LiteralModel> Find(string argName,
            string opName,
            IImmutableList<ArgumentModel> args,
            TypeModel t,
            IImmutableList<RunArgModel> context
            )
            => args.Where(m => m.Name == argName)
            .MaybeFirst()
            .toResult($"No argument found for {opName} called {argName}. ")
            .Map(arg => new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Body = arg.Expression,
                    Type = t
                },
                RunArgs = context
            })
            .FlatMap(executable => new ExecEvaluator().Execute(executable));


        /// <summary>
        /// get the specified boolean argument from the list of arguments.
        /// </summary>
        /// <param name="argName"></param>
        /// <param name="operationName"></param>
        /// <param name="models"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        protected Result<bool> FindBoolean(string argName
            , string operationName
            , IImmutableList<ArgumentModel> models
            , IImmutableList<RunArgModel> context
            )
            => models.Where(m => m.Name == argName)
                .MaybeFirst() // Maybe<ArgumentModel>
                .toResult($"No argument found for \"{operationName}\" called \"{argName}\".") // Result<ArugmentModel>
                .FlatMap(m => new ExpEvaluator().Boolean(m.Expression, context))
                ;

        protected Result<LambdaModel> FindFunction(string argName, 
            string opName, 
            IImmutableList<ArgumentModel> args,
            IImmutableList<RunArgModel> context,
            TypeModel returnType)
            => args.Where(m => m.Name == argName)
            .MaybeFirst()
            .toResult(errorMessage(opName, argName))
            .FlatMap(arg => 
                ExpEvaluator.Function(arg.Expression, context, returnType));


        protected Result<LiteralModel> FindList(string argName,
            string opName,
            IImmutableList<ArgumentModel> args,
            IImmutableList<RunArgModel> context,
            TypeModel t)
            => args.Where(m => m.Name == argName)
            .MaybeFirst()
            .toResult(errorMessage(opName, argName))
            .FlatMap(arg => ExpEvaluator.List(arg.Expression, context, t));

        /// <summary>
        /// get the specified numeric argument from the list of arguments.
        /// </summary>
        /// <param name="argName"></param>
        /// <param name="operationName"></param>
        /// <param name="models"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        protected Result<decimal> FindNumber(string argName
            , string operationName
            , IImmutableList<ArgumentModel> models
            , IImmutableList<RunArgModel> context)
            => models.Where(m => m.Name == argName)
                .MaybeFirst() // Maybe<ArgumentModel>
                .toResult($"No argument found for \"{operationName}\" called \"{argName}\".") // Result<ArugmentModel>
                .FlatMap(m => new ExpEvaluator().Number(m.Expression, context))
                ;

        /// <summary>
        /// get the specified string argument from the list of arguments.
        /// </summary>
        /// <param name="argName"></param>
        /// <param name="operationName"></param>
        /// <param name="models"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        protected Result<string> FindString(string argName
            , string operationName
            , IImmutableList<ArgumentModel> models
            , IImmutableList<RunArgModel> context)
            => models.Where(m => m.Name == argName)
                .MaybeFirst() // Maybe<ArgumentModel>
                .toResult($"No argument found for \"{operationName}\" called \"{argName}\".") // Result<ArugmentModel>
                .FlatMap(m => new ExpEvaluator().String(m.Expression, context))
                ;

        /*-------------------------helpers--------------------*/
        private string errorMessage(string opName, string argName)
            => $"No argument found for \"{opName}\" called \"{argName}\". ";
    }

}
