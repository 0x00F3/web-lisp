﻿using ApiModel;
using Core;
using System;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Evaluation
{
    public class ExpEvaluator
    {

        public Result<bool> Boolean(ExpressionModel expr
            , IImmutableList<RunArgModel> context
            )
            => LiteralEvaluator.Boolean(expr.Name)
            .RecoverWith(() => Parameter.Boolean(expr.Name, context))
            .RecoverWith(() => new ArgumentEvaluator().Boolean(expr, context))
            ;
            

        public Result<DateTime> Date(ExpressionModel expr
           , IImmutableList<RunArgModel> context
           )
           => LiteralEvaluator.Date(expr.Name)
            .RecoverWith(() => Parameter.Date(expr.Name, context))
            .RecoverWith(() => new ArgumentEvaluator().Date(expr, context))
            ;

        static internal Result<LambdaModel> Function(ExpressionModel expr,
            IImmutableList<RunArgModel> context,
            TypeModel returnType)
            => expr.Arguments.Where(arg => arg.Name == "expression")
            .MaybeFirst().Map(arg => arg.Expression)
            .Map(e =>  new DefinitionModel
            {
                Body = e,
                Type = returnType
            })
            .toResult("Lambdas require 'expression' argument")
            .Map(dfn => new LambdaModel(dfn, expr.Types));

        /// <summary>
        /// evaluator for list literals
        /// </summary>
        /// <param name="expr">an expression model named "list"</param>
        /// <param name="context">the runtime arguments in the current context</param>
        /// <param name="type">the type parameter of the list</param>
        /// <returns></returns>
        internal static Result<LiteralModel> List(ExpressionModel expr
            , IImmutableList<RunArgModel> context
            , TypeModel type)
            => LiteralEvaluator.List(expr, type, context)
            .RecoverWith(() => Parameter.List(expr.Name, context))
            .RecoverWith(() => new ArgumentEvaluator().List(expr, context, type))
            ;


        public Result<decimal> Number(ExpressionModel model
            , IImmutableList<RunArgModel> context
            )
            => LiteralEvaluator.Number(model.Name)
            .RecoverWith(() => Parameter.Number(model.Name, context))
            .RecoverWith(() => new ArgumentEvaluator().Number(model, context))
            ;

        internal Result<string> String(ExpressionModel model
            , IImmutableList<RunArgModel> context
            )
            => model.Either()
                .Match(args => new ArgumentEvaluator()
                    .String(model, context)
                , literal => new Result<string>(literal.Value)
                );

        internal Result<LiteralModel> Tuple(ExpressionModel expr
            , IImmutableList<RunArgModel> context,
            IImmutableList<TypeModel> types
            )
            => LiteralEvaluator.Tuple(expr, types, context)
            .RecoverWith(() => Parameter.Tuple(expr.Name, context))
            .RecoverWith(() => new ArgumentEvaluator().Tuple(expr, context));
    }
}
