﻿using ApiModel;
using Core;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Evaluation
{
    public class LiteralEvaluator
    {
        public Result<bool> Boolean(LiteralModel literal)
            => literal.Value.ParseBool()
                .toResult($"Could not parse \"{literal.Value}\" as literal boolean. ");

        public Result<DateTime> Date(LiteralModel literal)
            => literal.Value.ParseDate()
                .toResult($"Could not parse \"{literal.Value}\" as literal date. ");

        public static Result<bool> Boolean(string literal)
            => literal.ParseBool()
                .toResult($"Could not parse \"{literal}\" as literal boolean. ");

        public static Result<DateTime> Date(string literal)
            => literal.ParseDate()
                .toResult($"Could not parse \"{literal}\" as literal date. ");

        public static Result<LiteralModel> List(ExpressionModel expr,
            TypeModel t,
            IImmutableList<RunArgModel> context)
            => expr.Name == "list"
            ? expr.Expressions.Select(e => new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Body = e,
                    Type = t
                },
                RunArgs = context
            })
            .Select(dfn => new ExecEvaluator().Execute(dfn))
            .Invert()
            .Map(literals => new LiteralModel { Values = literals })
            : new Result<LiteralModel>(ImmutableList.Create("List literals must be named 'list'. "));

        public static Result<LiteralModel> Tuple(ExpressionModel expr,
            IImmutableList<TypeModel> types,
            IImmutableList<RunArgModel> context)
            => expr.Name == "tuple"
            ? tuple(expr.Expressions, types, context)
            : new Result<LiteralModel>(ImmutableList.Create("Tuple literals must be named 'tuple'. "));

        public static Result<decimal> Number(string literal)
            => literal.ParseDecimal()
                .toResult($"Could not parse \"{literal}\" as literal number. ")
            ;

        /* helpers */
        private static Result<LiteralModel> tuple(
            IImmutableList<ExpressionModel> expressions, 
            IImmutableList<TypeModel> types,
            IImmutableList<RunArgModel> context)
        {
            int k = types.Count();
            IImmutableList<Result<LiteralModel>> results = ImmutableList
                .Create<Result<LiteralModel>>();
            for (int i = 0; i < k; i++)
            {
                TypeModel t = types.ElementAt(i);
                results = results.Add(expressions.MaybeElementAt(i)
                    .toResult("Tuple doesn't have enough elements to satisfy type. ")
                    .Map(expr => new ExecutionModel
                    {
                        Dfn = new DefinitionModel
                        {
                            Body = expr,
                            Type = t
                        },
                        RunArgs = context
                    })
                    .FlatMap(executable => new ExecEvaluator().Execute(executable)))
                    ;
            }

            return results.Invert().Map(elements => new LiteralModel
            {
                Value = "tuple",
                Values = elements
            });
        }

    }
}
