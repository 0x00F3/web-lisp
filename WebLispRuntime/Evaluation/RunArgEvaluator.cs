﻿using ApiModel;
using Core;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Evaluation
{
    internal class RunArgEvaluator
    {
        internal Maybe<decimal> ResolveNumber(IImmutableList<RunArgModel> range, string target)
            => range.Where(r => r.Name == target).MaybeFirst()
                .FlatMap(r => r.AsNumber())
                ;

        internal Maybe<bool> ResolveBoolean(IImmutableList<RunArgModel> range, string target)
            => range.Where(r => r.Name == target).MaybeFirst()
                .FlatMap(r => r.AsBoolean())
                ;
    }
}
