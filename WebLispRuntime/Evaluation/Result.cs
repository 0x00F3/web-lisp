﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Evaluation
{
    /// <summary>
    /// compilation result. Contains either an instance of T or an
    /// IImmutableList of error messages. 
    /// </summary>
    public class Result<T> 
    {
        private readonly IImmutableList<string> errors = ImmutableList.Create<string>();
        private readonly T model;



        public Result(IImmutableList<string> errors)
        {
            this.errors = errors ?? throw new ArgumentNullException(nameof(errors));
        }

        public Result(T model)
        {
            this.model = model;
        }

        public Result(T model, IImmutableList<string> errors)
        {
            this.errors = errors
                ?? throw new ArgumentNullException(nameof(errors));
            this.model = model;
        }

        /// <summary>
        /// adds errors if there are any, else leaves the result unchanged.
        /// </summary>
        /// <param name="errors"></param>
        /// <returns></returns>
        public Result<T> AddErrors(IImmutableList<string> errors)
            => errors.Any()
            ? new Result<T>(this.errors.AddRange(errors))
            : this
            ;


        public IImmutableList<string> Errors() => errors;

        public Result<TNext> FlatMap<TNext>(Func<T, Result<TNext>> f)
            => errors.Any() ? new Result<TNext>(errors) : f(model);


        public Result<TNext> Map<TNext>(Func<T, TNext> f)
            => errors.Any()
            ? new Result<TNext>(errors)
            : new Result<TNext>(f(model));

        public TNext Match<TNext>(Func<T, TNext> success, Func<IImmutableList<string>, TNext> failed)
            => errors.Any()
            ? failed(errors)
            : success(model)
            ;

        public Result<T> RecoverWith(Func<Result<T>> f)
            => Match(model => this, errors => f());

    }
}
