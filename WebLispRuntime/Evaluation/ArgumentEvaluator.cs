﻿using ApiModel;
using Evaluation.Operation.Boolean;
using Evaluation.Operation.Generic;
using Evaluation.Operation.List;
using Evaluation.Operation.Number;
using System;
using System.Collections.Immutable;

namespace Evaluation
{
    internal class ArgumentEvaluator
    {
        
        internal Result<bool> Boolean(ExpressionModel expr
            , IImmutableList<RunArgModel> context
            )
            => 
            expr.Name == "and" ? new Conjunction().Evaluate(expr.Arguments, context)
            : expr.Name == "congruent" ? new Congruence().Evaluate(expr.Arguments, context)
            : expr.Name == "equal" ? new Equality().Evaluate(expr.Arguments, context)
            : expr.Name == "eval" ? new Parameter().Boolean(expr.Arguments, context)
            : expr.Name == "greater-than" ? new Magnitude().Evaluate(expr.Arguments, context)
            : expr.Name == "not" ? new Inversion().Evaluate(expr.Arguments, context)
            : expr.Name == "or" ? new Disjunction().Evaluate(expr.Arguments, context)
            // HACK is there a better way than going back and forth like this
            // between strings and primitives? 
            : generic(expr, context, new TypeModel("boolean"))
                .FlatMap(value => new LiteralEvaluator().Boolean(value))
            ;

        internal Result<DateTime> Date(ExpressionModel expr
            , IImmutableList<RunArgModel> context
            )
            => generic(expr, context, new TypeModel("date"))
                .FlatMap(value => new LiteralEvaluator().Date(value));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationName">name of the expression</param>
        /// <param name="args">the arguments of the expression</param>
        /// <param name="context">runtime arguments in the current context</param>
        /// <param name="type">the type parameter of the list</param>
        /// <returns></returns>
        internal Result<LiteralModel> List(ExpressionModel expr
            , IImmutableList<RunArgModel> context
            , TypeModel type)
            => expr.Name == "map" ? new Projection().Evaluate(expr, context, type)
            : expr.Name == "append" 
            ? new Operation.List.Addition().Evaluate(expr.Arguments, context, type)
            : expr.Name == "filter"
            ? new Filtration().Evaluate(expr, context, type)
            : generic(expr, context, type);

        internal Result<decimal> Number(ExpressionModel expr
            , IImmutableList<RunArgModel> context
            )
            => expr.Name == "add"
            ? new Operation.Number.Addition().Evaluate(expr.Arguments, context)
            : expr.Name == "divide" ? new Division().Evaluate(expr.Arguments, context)
            : expr.Name == "multiply"
            ? new Multiplication().Evaluate(expr.Arguments, context)
            : expr.Name == "subtract"
            ? new Subtraction().Evaluate(expr.Arguments, context)
            : generic(expr, context, new TypeModel("number"))
                .FlatMap(literal => LiteralEvaluator.Number(literal.Value));

        internal Result<string> String(ExpressionModel expr
            , IImmutableList<RunArgModel> context
            )
            => expr.Name == "eval" ? new Parameter().String(expr.Arguments, context)
            : generic(expr, context, new TypeModel("string"))
                .FlatMap(literal => new Result<string>(literal.Value));

        internal Result<LiteralModel> Tuple(ExpressionModel expr
            , IImmutableList<RunArgModel> context)
            => generic(expr, context, new TypeModel("tuple"))
            ;

        private Result<LiteralModel> generic(ExpressionModel expr
            , IImmutableList<RunArgModel> context
            , TypeModel t)
            => expr.Name == "apply" ? new Application().Evaluate(expr, context, t)
            : expr.Name == "head" ? new Head().Evaluate(expr.Arguments, context, t)
            : expr.Name == "if" ? new Conditional().Evaluate(expr.Arguments, context, t)
            : expr.Name == "reduce" ? new Reduction().Evaluate(expr, context, t)
            : new Result<LiteralModel>(
                ImmutableList.Create(noSuchInstruction(t, expr.Name))
                )
            ;

        /*-------------------------error messages----------------------------*/
        private string noSuchInstruction(TypeModel t, string operation)
            => $"No operation called \"{operation}\" yields type \"{t.Name}\". ";

        private string typeParameterRequired(TypeModel outerType)
            => $"Type \"{outerType.Name}\" requres a generic inner type, but none was provided. ";
    }
}
