﻿using Core;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Evaluation
{
    public static class MaybeExtensions
    {
        public static Result<T> toResult<T>(this Maybe<T> maybe, string error)
            => maybe.Match(some => new Result<T>(some)
            , () => new Result<T>(ImmutableList.Create(error))
            );
    }
}
