﻿using ApiModel;
using Core;
using System;
using System.Collections.Immutable;
using System.Linq;

namespace Evaluation
{
    /// <summary>
    /// evaluates a runtime argument name as a parameter
    /// </summary>
    public class Parameter
    {
        public Result<bool> Boolean(IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
        )
        => args.MaybeFirst()
                .toResult("No value found for operation \"eval\". ")
                .Map(arg => arg.Value)
                .FlatMap(name => find(name, context))
                .Map(runArg => runArg.Literal)
                .FlatMap(literal => LiteralEvaluator.Boolean(literal.Value))
            ;

        //public Result<LiteralModel> List(
        //    IImmutableList<ArgumentModel> args
        //    , IImmutableList<RunArgModel> context
        //    )
        //    => args.MaybeFirst()
        //        .toResult("No value found for operation \"eval\". ")
        //        .Map(arg => arg.Value)
        //        .FlatMap(name => find(name, context))
        //        .Map(runArg => runArg.Literal)
        //    ;

        public Result<decimal> Number(IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
        )
        => args.MaybeFirst()
                .toResult("No value found for operation \"eval\". ")
                .Map(arg => arg.Value)
                .FlatMap(name => find(name, context))
                .Map(runArg => runArg.Literal)
                .FlatMap(literal => LiteralEvaluator.Number(literal.Value))
            ;

        public Result<string> String(IImmutableList<ArgumentModel> args
            , IImmutableList<RunArgModel> context
            )
            => args.MaybeFirst()
                .toResult("No value found for operation \"eval\". ")
                .Map(arg => arg.Value)
                .FlatMap(name => find(name, context))
                .Map(runArg => runArg.Literal.Value);

        //internal Result<ExpressionModel> Tuple(IImmutableList<ArgumentModel> args
        //    , IImmutableList<RunArgModel> context
        //    )
        //    => args.MaybeFirst()
        //        .toResult("No value found for operation \"eval\". ")
        //        .Map(arg => arg.Value)
        //        .FlatMap(name => find(name, context))
        //        .Map(runArg => runArg.Literal);

        public static Result<bool> Boolean(string parameterName
            , IImmutableList<RunArgModel> context
        )
        => find(parameterName, context)
                .Map(runArg => runArg.Literal)
                .FlatMap(literal => LiteralEvaluator.Boolean(literal.Value))
            ;

        public static Result<DateTime> Date(string parameterName
            , IImmutableList<RunArgModel> context
            )
            => find(parameterName, context)
                .Map(runArg => runArg.Literal)
                .FlatMap(literal => LiteralEvaluator.Date(literal.Value))
            ;

        public static Result<decimal> Number(string parameterName
            , IImmutableList<RunArgModel> context
        )
        => find(parameterName, context)
                .Map(runArg => runArg.Literal)
                .FlatMap(literal => LiteralEvaluator.Number(literal.Value))
            ;

        public static Result<LiteralModel> List(string parameterName
            , IImmutableList<RunArgModel> context
            )
            => find(parameterName, context)
                .Map(runArg => runArg.Literal)
            ;

        public static Result<LiteralModel> Tuple(string parameterName,
            IImmutableList<RunArgModel> context)
            => find(parameterName, context).Map(runarg => runarg.Literal);

        private static Result<RunArgModel> find(string name
            , IImmutableList<RunArgModel> context
            )
            => context.Where(runArg => runArg.Name == name)
                .MaybeFirst()
                .toResult($"Unable to find runtime argument \"{name}\" in the current context. ")
            ;
    }
}
