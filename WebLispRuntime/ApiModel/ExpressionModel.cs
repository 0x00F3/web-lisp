﻿using Core;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace ApiModel
{
    public class ExpressionModel
    {
        public string Name { get; set; }

        /// <summary>
        /// THIS IS ONLY PUBLIC SO THE MODEL BINDER CAN USE IT. DO NOT 
        /// REFERENCE. (except in the test suite). USE THE Either!
        /// </summary>
        public IImmutableList<ArgumentModel> Arguments { get; set; }

        private IImmutableList<ExpressionModel> expressions;
        public IImmutableList<ExpressionModel> Expressions
        {
            get => expressions ?? (expressions = ImmutableList.Create<ExpressionModel>());
            set => expressions = value;
        }


        private IImmutableList<ParameterModel> types;
        /// <summary>
        /// fills in generics of function call, if applicable.
        /// </summary>
        public IImmutableList<ParameterModel> Types
        {
            get => types ?? (types = ImmutableList.Create<ParameterModel>());
            set => types = value;
        }


        private LiteralModel literal;
        public LiteralModel Literal
        {
            get => literal ?? (literal = new LiteralModel());
            set => literal = value;
        }

        public ExpressionModel() { }

        /// <summary>
        /// This does not validate the object as having ONLY one or the other.
        /// Please validate separately. 😞
        /// </summary>
        /// <returns></returns>
        public Either<IImmutableList<ArgumentModel>, LiteralModel> Either()
            => Arguments == null
            ? new Either<IImmutableList<ArgumentModel>, LiteralModel>(Literal)
            : new Either<IImmutableList<ArgumentModel>, LiteralModel>(Arguments);
    }
}
