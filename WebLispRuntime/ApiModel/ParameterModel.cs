﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiModel
{
    public class ParameterModel
    {
        public string Name { get; set; }
        public int Index { get; set; }
        public TypeModel Type { get; set; }

    }
}
