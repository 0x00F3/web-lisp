﻿using Core;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace ApiModel
{
    public class RunArgModel
    {
        private string name;
        public string Name { get => name ?? ""; set => name = value; }

        private LiteralModel literal;
        public LiteralModel Literal
        {
            get => literal ?? (literal = new LiteralModel { Value = "" });
            set => literal = value;
        }


        public RunArgModel() { }

        public RunArgModel(string name, string value)
        {
            Name = name;
            Literal = new LiteralModel { Value = value };
        }

        public RunArgModel(string name, IImmutableList<LiteralModel> values)
        {
            Name = name;
            Literal = new LiteralModel
            {
                Values = values
            };
        }

        public RunArgModel(string name, LiteralModel model)
        {
            Name = name;
            Literal = model;
        }
        
        
        public Maybe<bool> AsBoolean()
            => Literal.Value.ParseBool();

        public Maybe<decimal> AsNumber()
            => Literal.Value.ParseDecimal();

    }
}
