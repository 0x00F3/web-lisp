﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace ApiModel
{
    public class ExecutionModel
    {
        public DefinitionModel Dfn { get; set; }

        private IImmutableList<RunArgModel> runArgs;
        public IImmutableList<RunArgModel> RunArgs
        {
            get => runArgs ?? ImmutableList.Create<RunArgModel>();
            set => runArgs = value;
        }
    }
}
