﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiModel
{
    public class ArgumentModel
    {
        public string Name { get; set; }
        public ExpressionModel Expression { get; set; }

        private TypeModel type;
        public TypeModel Type
        {
            get => type ?? new TypeModel();
            set => type = value;
        }

        private string value;
        public string Value
        {
            get => value ?? "";
            set => this.value = value;
        }

    }
}
