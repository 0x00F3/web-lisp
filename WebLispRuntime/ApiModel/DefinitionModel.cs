﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace ApiModel
{
    public class DefinitionModel
    {
        public TypeModel Type { get; set; }
        public ExpressionModel Body { get; set; }
    }
}
