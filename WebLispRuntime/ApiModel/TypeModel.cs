﻿using Core;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace ApiModel
{
    public class TypeModel
    {
        public string Name { get; set; }

        private IImmutableList<TypeModel> generics;

        //represents a generic
        public IImmutableList<TypeModel> Generics
        {
            get => generics ?? (generics = ImmutableList.Create<TypeModel>());
            set => generics = value;
        }

        public TypeModel() {  }

        public TypeModel(string name)
        {
            Name = name;
        }

        public TypeModel(string name, TypeModel t)
        {
            Name = name;
            Generics = ImmutableList.Create(t);
        }

        public Maybe<TypeModel> MaybeT()
            => Generics.MaybeFirst();
    }
}
