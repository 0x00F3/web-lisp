﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ApiModel;
using Evaluation;
using Microsoft.AspNetCore.Mvc;
using WebLispRuntime.Models;

namespace WebLispRuntime.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index([FromBody] ExecutionModel executable)
        {
            ResultModel result = new ExecEvaluator().Execute(executable)
                .Match(literal => new ResultModel(literal)
                , errors => new ResultModel(errors));
            return Json(result);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
