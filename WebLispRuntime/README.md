# README
## Endpoint Structure
### Representing Types with TypeModels
`TypeModel`s have two members. The Name--a string value representing the type--
is required. The other property, the T value, is another `TypeModel`. This type
is optional, used only when representing a generic.

The following is an exhaustive list of valid `TypeModel`s.
* Name = "boolean"
* Name = "date"
* Name = "list" **and** the T value is required to be another `TypeModel`.
* Name = "number"
* Name = "string"
* Name = "tuple"

"list" is currently the only type that represents generics. The `TypeModel` T
is required for lists--passing a null will always result in an error. Just 
like in other typesystems, matrices can be represented as lists of lists of 
(whatever). For all other types, the `TypeModel` T of the `TypeModel` will 
not be used. 

(For more information on "tuple"s and the logic engine typesystem, see the 
forthcoming section of "tuple"s.)

TypeModels can also be used to define input types of methods that take
generic arguments. For example, if mapping a list of booleans to a list of 
numbers, the "boolean" type will be passed as a parameter to the "map"
operation. The "number" type will be inferred from the usage. See the section
on ArgumentExpressions for more details. 

Lambda expressions are also supported by the typesystem in a limited way, but
lambda types are always inferred by the context. There is no explicity way to
define a lambda type using TypeModels, so lambda types are out of scope for
this section on TypeModels. See the section on ExpressionModels for more
information on lambda expressions. 

### Representing Literals with LiteralModels
`LiteralModel`s are composed of two parts: a string value and a list of other
`LiteralModel`s. These two pieces can be composed to represent a literal of any
type that the logic engine can represent. 

* **boolean** literals are represented by "True" or "False" in the Value
property and an empty (or null) list in the Values property.
* **date** literals are represented by an ISO8601 string in the Value property.
* **list** literals are represented by an empty (or null) string in the Value
property and the contents of the list in the Value property.
* **number** literals are represented by a string which is parsable as a 
decimal in the Value property and an empty (or null) list in the Values
property.
* **string** literals are represented by the string in the Value property
and an empty (or null) list in the Values property.
* **tuple** literals use both properties: a tuple name that uniquely identifies
the tuple in the Value property and the tuple members in the Values property.

### DefinitionModels: the Root of the Expression Tree
In order to begin evaluating an expression, you have to know the type of that
expression. The DefinitionModel represents both through a `TypeModel` Type and
`ExpressionModel` expression Body. Since `ExpressionModel`s are an object 
structure that's corecursive with `ArgumentModel`s, the Body property 
represents the root of the conceptual expression tree.

### ExpressionModels and ArgumentModels
`ExpressionModel`s and `ArgumentModel`s are corecursive objects. That is, 
expressions can have arguments, and arguments can have an expression. These two
classes together form an expression tree. 

Either of these classes can be a leaf-node of the tree, though. To break it 
down, 

#### ExpressionModels
`ExpressionModel`s have two possible states: 
* If the Expression has a **null** Arguments list, it means the real value
is in the Literal. This case is the leaf-node case for Expressions. 
* Otherwise, the Expression is expected to have a string Name that represents
the command of the operation. This case represents a branch of the tree, and
evaluation will continue through each `ArgumentModel` in the list. 

**IMPORTANT NOTE**: A **null** Arguments list means something completely
different here than an **empty** Arguments list. If you want the Expression
to be read for its Literal instead of its Arguments, then the Arguments list
**has to be null** not **empty**. If the list of Arguments list is **empty**, 
it will be processed as an expression without arguments, which will ultimately
result in error because all expressions have some required arguments.

If needed, `ExpressionModel`s can be used to represent **lambdas**. An
expression is read as a lambda if the parent argument of the expression is
defined to be a lambda by the expression definition (For example, for the 
expression "filter", the argument "f" will be read for its Expression, and 
that `ExpressionModel` will be treated as a lambda.) Like in other languages,
lambdas have their own scoped parameters (e. g., the "map" expression a
lambda-typed argument called "f". Within f's scope, the client can refer to 
the current element from the list by the element name defined through another
argument. See test suite for examples.)

#### ArgumentModels
`ArgumentModel`s have three possible states. 

* If executing an "eval" operation (i. e., if the instance of `ArgumentModel`
is inside an ExpressionModel whose Name is "eval"), then the `ArgumentModel` is
treated as a leaf. The string Value property is read as the name of a 
`RunArgModel`, and the Expression is not used. 
* If the engine is expecting the argument to be a type(e. g., the "tprev"
argument of the "map" operation on lists) the argument will be read for its 
Type and neither the Expression nor the Value will be used. 
* Otherwise, the `ArgumentModel` is treated as a leaf-node and evaluation of
the expression tree continues through the Expression property, and the Value
property is not used.

## Representing Data Passed to an Expression Using RunArgModels
`RunArgModel`s have two members-- a `RunArgModel` knows its name as a string
value by which it will be referenced in an ArgumentModel, and it knows its 
value, represented by a `LiteralModel`.

### Putting it all together: ExecutionModels

To execute an expression with a given set of values, the logic engine endpoint 
needs an `ExecutionModel`. The `ExecutionModel` has two parts: a 
`DefinitionModel` represents the logical expression and the type it should 
resolve to and an a list of `RunArgModel`s that represent the data that the
expression should run against. The logic engine's ultimate purpose is to 
evaluate the Definition's Expression as the Definition's Type in the context 
of the RunArgs.

## Valid Expressions and Their Arguments
The following faux "function signatures" represent valid Expression names and
the named Arguments they require. Unlike most languages, the order that 
arguments are listed doesn't matter. Instead, the logic engine relies on the
Name property of the argument. 

### boolean
- [x] and(first:boolean, second:boolean)
- [ ] before(first:date, second:date)
- [x] congruent(first:string, second:string)
- [x] equal(first:number, second:number)
- [x] greater-than(first:number, second:number)
- [x] not(operand:boolean)
- [x] or(first:boolean, second:boolean)
- [ ] synchronous(first:date, second:date)

### list of T
- [x] append(list:list T, element:T)
- [x] filter(operand:list T, f:T => boolean)
- [x] map(tprev:Type, operand:list TPrev, f:(TPrev => T))

### number
- [x] add(addend:number, augend:number)
- [x] divide(dividend:number, divisor:number)
- [x] multiply(multiplicand:number, multiplier:number)
- [x] subtract(minuend:number, subtrahend:number)

### T (generic)
- [x] apply(operand:tuple, f:(tuple types in order => T))
- [x] if(condition:boolean, pass:T, fail:T)
- [x] head(operand:list)
- [ ] reduce

## Next Priorities

## Potential Refactors
- [ ] optimize memory by removing empty constructors in favor of static members
- [ ] get rid of literal evaluator in favor of methods on LiteralModel
- [ ] get rid of noun-based naming convention (e. g. replace "Disjunction.cs" 
	with "Or.cs")



