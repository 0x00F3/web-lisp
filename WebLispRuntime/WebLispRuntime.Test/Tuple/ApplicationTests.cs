﻿using ApiModel;
using Evaluation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace WebLispRuntime.Test.Tuple
{
    [TestClass]
    public class ApplicationTests
    {
        /// <summary>
        /// sanity check
        /// </summary>
        [TestMethod]
        public void TestRuntimeArgument()
        {
            // arrange
            ExecutionModel executable = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("tuple"),
                    Body = new ExpressionModel
                    {
                        Name = "arg"
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("arg", new LiteralModel
                {
                    Value = "mytuple", // tuple name
                    Values = ImmutableList.Create(new LiteralModel("hello"))
                        .Add(new LiteralModel("world"))
                }))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(executable);

            // assert
            result.Match(literal =>
            {
                Assert.AreNotEqual("arg", literal.Value);
                int successes = literal.Values
                    .Where(lit => lit.Value == "hello" || lit.Value == "world")
                    .Count();
                Assert.AreEqual(2, successes);
                Assert.AreEqual(successes, literal.Values.Count);
                return 0;
            }
            , errors =>
            {
                Assert.Fail();
                return -1;
            });
        }

        [TestMethod]
        public void TestApply()
        {
            // arrange
            ExecutionModel executable = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("boolean"),
                    Body = new ExpressionModel
                    {
                        Name = "apply",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "operand",
                            Expression = new ExpressionModel
                            {
                                Name = "tuple",
                                Expressions = 
                                    ImmutableList.Create(new ExpressionModel
                                    {
                                        Name = "70.72"
                                    })
                                    .Add(new ExpressionModel
                                    {
                                        Name = "69.37"
                                    })
                            }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "f",
                            Expression = new ExpressionModel
                            {
                                Name = "lambda",
                                Arguments = ImmutableList.Create(
                                    new ArgumentModel
                                    {
                                        Name = "expression",
                                        Expression = new ExpressionModel
                                        {
                                            Name = "greater-than",
                                            Arguments = ImmutableList.Create(new ArgumentModel
                                            {
                                                Name = "first",
                                                Expression = new ExpressionModel
                                                {
                                                    Name = "my-tuple._1"
                                                }
                                            })
                                            .Add(new ArgumentModel
                                            {
                                                Name = "second",
                                                Expression = new ExpressionModel
                                                {
                                                    Name = "my-tuple._2"
                                                }
                                            })
                                        }
                                    }),
                                Types = ImmutableList.Create(new ParameterModel
                                {
                                    Index = 0,
                                    Name = "my-tuple._1",
                                    Type = new TypeModel("number")
                                })
                                .Add(new ParameterModel
                                {
                                    Index = 1,
                                    Name = "my-tuple._2",
                                    Type = new TypeModel("number")
                                })
                                .Add(new ParameterModel
                                {
                                    Index = 2,
                                    Name = "TRETURN",
                                    Type = new TypeModel("number")
                                })
                            }
                        }),
                        Types = ImmutableList.Create(new ParameterModel
                        {
                            Index = 0,
                            Name = "T1",
                            Type = new TypeModel("number")
                        })
                        .Add(new ParameterModel
                        {
                            Index = 0,
                            Name = "T2",
                            Type = new TypeModel("number")
                        })
                        
                    }
                }
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(executable);

            // assert
            result.Match(literal =>
            {
                Assert.AreEqual(true.ToString(), literal.Value);
                return 0;
            }, errors =>
            {
                var failures = errors.Select(e =>
                {
                    Assert.Fail(e);
                    return -1;
                })
                .ToImmutableList(); // force execution. 
                /* Have I mentioned how much
                 * I still hate lazy evaluation? */

                return -1;
            });
        }

    }
}
