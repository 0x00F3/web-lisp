﻿using ApiModel;
using Evaluation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace WebLispRuntime.Test.Boolean
{
    [TestClass]
    public class OrTests
    {
        [TestMethod]
        public void TestOrFalse()
        {
            // arrange
            ApiModel.ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("boolean"),
                    Body = new ExpressionModel
                    {
                        Name = "or",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "first",
                            Expression = new ExpressionModel { Name = "true" }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "second",
                            Expression = new ExpressionModel
                            {
                                Name = "condition"
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("condition", "false"))
            };

            //act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);
            result.Match(answer =>
            {
                Assert.AreEqual("True", answer.Value);
                return 0;
            }
            , errors =>
            {
                Assert.Fail();
                return -1;
            });
        }

        [TestMethod]
        public void TestOrTrue()
        {
            // arrange
            ApiModel.ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("boolean"),
                    Body = new ExpressionModel
                    {
                        Name = "or",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "first",
                            Expression = new ExpressionModel { Name = "false" }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "second",
                            Expression = new ExpressionModel
                            {
                                Name = "condition"
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("condition", "true"))
            };

            //act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);
            result.Match(answer =>
            {
                Assert.AreEqual("True", answer.Value);
                return 0;
            }
            , errors =>
            {
                Assert.Fail();
                return -1;
            });
        }

        [TestMethod]
        public void TestBothFalse()
        {
            // arrange
            ApiModel.ExecutionModel executable = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("boolean"),
                    Body = new ExpressionModel
                    {
                        Name = "or",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "first",
                            Expression = new ExpressionModel { Name = "false" }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "second",
                            Expression = new ExpressionModel
                            {
                                Name = "condition"
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("condition", "false"))
            };

            //act
            Result<LiteralModel> result = new ExecEvaluator().Execute(executable);
            result.Match(answer =>
            {
                Assert.AreEqual("False", answer.Value);
                return 0;
            }
            , errors =>
            {
                Assert.Fail();
                return -1;
            });
        }
        
    }
}
