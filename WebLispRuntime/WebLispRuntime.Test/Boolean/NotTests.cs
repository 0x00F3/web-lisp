﻿using ApiModel;
using Evaluation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace WebLispRuntime.Test.Boolean
{
    /// <summary>
    /// a class to test the "not" boolean operation. 
    /// </summary>
    [TestClass]
    public class NotTests
    {
        [TestMethod]
        public void TestNotConstant()
        {
            // arrange
            ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("boolean"),
                    Body = new ExpressionModel
                    {
                        Name = "not",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "operand",
                            Expression = new ExpressionModel { Name = "true"}
                        })
                    }
                },
                RunArgs = ImmutableList.Create<RunArgModel>() 
            };

            //act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);
            result.Match(answer =>
            {
                Assert.AreEqual("False", answer.Value);
                return 0;
            }
            , errors =>
            {
                Assert.Fail();
                return -1;
            });
        }

        [TestMethod]
        public void TestNotArgument()
        {
            // arrange
            ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("boolean"),
                    Body = new ExpressionModel
                    {
                        Name = "not",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "operand",
                            Expression = new ExpressionModel
                            {
                                Name = "eval",
                                Arguments = ImmutableList.Create(new ArgumentModel
                                {
                                    Value = "k"
                                })
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("k", "false")) // there's a null pointer exception if RunArgs is null
            };

            //act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);
            result.Match(answer =>
            {
                Assert.AreEqual("True", answer.Value);
                return 0;
            }
            , errors =>
            {
                Assert.Fail();
                return -1;
            });
        }
    }
}
