﻿using ApiModel;
using Evaluation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace WebLispRuntime.Test.Boolean
{
    [TestClass]
    public class EqualTests
    {
        [TestMethod]
        public void TestNotEqual()
        {
            // arrange
            ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("boolean"),
                    Body = new ExpressionModel
                    {
                        Name = "equal",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "first",
                            Expression = new ExpressionModel { Name = "17.07" }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "second",
                            Expression = new ExpressionModel
                            {
                                Name = "i"
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("i", "81.92"))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);

            // assert
            Assert.AreEqual(0, result.Errors().Count);
            result.Match(answer =>
            {
                Assert.AreEqual("False", answer.Value);
                return 0;
            }
            , errors =>
            {
                Assert.Fail();
                return -1;
            });
        }

        [TestMethod]
        public void TestEqualTrue()
        {
            // arrange
            ExecutionModel executable = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("boolean"),
                    Body = new ExpressionModel
                    {
                        Name = "equal",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "first",
                            Expression = new ExpressionModel { Name = "4614.61" }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "second",
                            Expression = new ExpressionModel
                            {
                                Name = "i"
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("i", "4614.61"))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(executable);

            // assert
            Assert.AreEqual(0, result.Errors().Count);
            result.Match(answer =>
            {
                Assert.AreEqual("True", answer.Value);
                return 0;
            }
            , errors =>
            {
                Assert.Fail();
                return -1;
            });
        }
    }
}
