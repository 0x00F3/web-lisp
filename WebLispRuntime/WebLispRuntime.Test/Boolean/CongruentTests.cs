﻿using ApiModel;
using Evaluation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace WebLispRuntime.Test.Boolean
{
    [TestClass]
    public class CongruentTests
    {
        [TestMethod]
        public void TestNotCongruent()
        {
            // arrange
            ApiModel.ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("boolean"),
                    Body = new ExpressionModel
                    {
                        Name = "congruent",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "first",
                            Expression = new ExpressionModel
                            {
                                Literal = new LiteralModel("hello")
                            }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "second",
                            Expression = new ExpressionModel
                            {
                                Name = "eval",
                                Arguments = ImmutableList.Create(new ArgumentModel
                                {
                                    Value = "string2"
                                })
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("string2", "world"))
            };

            //act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);
            result.Match(answer =>
            {
                Assert.AreEqual("False", answer.Value);
                return 0;
            }
            , errors =>
            {
                Assert.Fail(JsonConvert.SerializeObject(errors));
                return -1;
            });
        }

        [TestMethod]
        public void TestCongruent()
        {
            // arrange
            ApiModel.ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("boolean"),
                    Body = new ExpressionModel
                    {
                        Name = "congruent",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "first",
                            Expression = new ExpressionModel
                            {
                                Literal = new LiteralModel("hello")
                            }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "second",
                            Expression = new ExpressionModel
                            {
                                Name = "eval",
                                Arguments = ImmutableList.Create(new ArgumentModel
                                {
                                    Value = "string2"
                                })
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("string2", "hello"))
            };

            //act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);
            result.Match(answer =>
            {
                Assert.AreEqual("True", answer.Value);
                return 0;
            }
            , errors =>
            {
                Assert.Fail(JsonConvert.SerializeObject(errors));
                return -1;
            });
        }
    }
}
