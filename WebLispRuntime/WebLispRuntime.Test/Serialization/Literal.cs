﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace ApiModel
{
    public class Literal
    {
        private string value;
        public string Value { get => value ?? ""; set => this.value = value; }

        private IList<Literal> values;
        public IList<Literal> Values
        {
            get => values ?? ImmutableList.Create<Literal>();
            set => values = value;
        }

        public Literal() { }

        public Literal(string value)
        {
            Value = value ?? "";
        }

        public Literal(IList<Literal> values)
        {
            Value = "";
            Values = values ?? ImmutableList.Create<Literal>();
        }

        /// <summary>
        /// tuple constructor
        /// </summary>
        /// <param name="value">tuple name</param>
        /// <param name="values"></param>
        public Literal(string value, IList<Literal> values)
        {
            Value = value;
            Values = values;
        }

    }
}
