﻿using ApiModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace WebLispRuntime.Test.Serialization
{
    [TestClass]
    public class LiteralSerializationTests
    {
        [TestMethod]
        public void TestNumericLiteralNewtonsoftSerialization()
        {
            // arrange
            Literal literal = new Literal("59");

            // act 
            string json = JsonConvert.SerializeObject(literal);

            // assert
            Assert.IsTrue(json.Contains("59"));
        }

        [TestMethod]
        public void TestNumericLiteralNewtonsoftDeserialization()
        {
            // arrange
            LiteralModel literal = new LiteralModel("59");
            string json = JsonConvert.SerializeObject(literal);

            // act
            LiteralModel sut = JsonConvert.DeserializeObject<LiteralModel>(json);

            // assert
            Assert.AreEqual("59", sut.Value);
        }

        [TestMethod]
        public void TestNumericLiteralDotNetDeserialization()
        {
            // arrange
            Literal literal = new Literal("59");
            string json = System.Text.Json.JsonSerializer.Serialize(literal);

            // act
            Literal sut = System.Text.Json.JsonSerializer.Deserialize<Literal>(json);

            // assert
            Assert.AreEqual("59", sut.Value);
        }
    }
}
