﻿using ApiModel;
using Evaluation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace WebLispRuntime.Test.Number
{
    [TestClass]
    public class TrivialExecutionTests
    {
        [TestMethod]
        public void TestInvalidType()
        {
            // arrange
            ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("foobar"), //this type will cause a failure. 
                    Body = new ExpressionModel
                    {
                        Name = "eval",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Value = "k"
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("k", "5"))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);

            // assert
            Assert.AreNotEqual(0, result.Errors().Count);
        }

        [TestMethod]
        public void TestUnrecognizedParameter()
        {
            // arrange
            ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("number"),
                    Body = new ExpressionModel
                    {
                        Name = "eval",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Value = "k"
                        })
                    } // unrecognized
                },
                RunArgs = ImmutableList.Create(new RunArgModel("i", "5"))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);

            // assert
            Assert.AreNotEqual(0, result.Errors().Count);
        }

        [TestMethod]
        public void TestConstant()
        {
            // arrange
            ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("number"),
                    Body = new ExpressionModel { Name = "i"}
                },
                RunArgs = ImmutableList.Create(new RunArgModel("i", "5"))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);

            // assert
            Assert.AreEqual(0, result.Errors().Count);
            result.Map(answer => {
                Assert.AreEqual("5", answer.Value);
                return true;
            });
        }

        [TestMethod]
        public void TestArgument()
        {
            // arrange
            ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("number"),
                    Body = new ExpressionModel
                    {
                        Name = "i"
                    } // unrecognized
                },
                RunArgs = ImmutableList.Create(new RunArgModel("i", "5"))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);

            // assert
            Assert.AreEqual(0, result.Errors().Count);
            result.Map(answer => {
                Assert.AreEqual("5", answer.Value);
                return true;
            });
        }

        [TestMethod]
        public void TestInvalidOperation()
        {
            // arrange
            ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("number"),
                    Body = new ExpressionModel
                    {
                        Name = "foobar", // unrecognized numeric operation
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Expression = new ExpressionModel { Literal = new LiteralModel("5") }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("i", "5"))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);

            // assert
            Assert.AreNotEqual(0, result.Errors().Count);
        }
    }
}
