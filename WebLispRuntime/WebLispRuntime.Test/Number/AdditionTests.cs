﻿using ApiModel;
using Evaluation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace WebLispRuntime.Test.Number
{
    [TestClass]
    public class AdditionTests
    {
        [TestMethod]
        public void TestMissingArgument()
        {
            // arrange
            ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("number"),
                    Body = new ExpressionModel
                    {
                        Name = "add",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "addend",
                            Expression = new ExpressionModel
                            {
                                Name = "eval",
                                Arguments = ImmutableList.Create(new ArgumentModel
                                {
                                    Value = "i"
                                })
                            }
                        }) // missing augend
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("i", "5"))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);

            // assert
            // didn't find argument for parameter augend.
            Assert.AreNotEqual(0, result.Errors().Count);
        }

        [TestMethod]
        public void TestAddToConstant()
        {
            // arrange
            ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("number"),
                    Body = new ExpressionModel
                    {
                        Name = "add",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "augend",
                            Expression = new ExpressionModel { Name = "2" }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "addend",
                            Expression = new ExpressionModel
                            {
                                Name = "i"
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("i", "5"))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);

            // assert
            Assert.AreEqual(0, result.Errors().Count);
            result.Match(answer =>
            {
                Assert.AreEqual("7", answer.Value);
                return 0;
            }
            , errors =>
            {
                Assert.Fail();
                return -1;
            });
        }

        [TestMethod]
        public void TestAddTwoArguments()
        {
            // arrange
            ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("number"),
                    Body = new ExpressionModel
                    {
                        Name = "add",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "augend",
                            Expression = new ExpressionModel
                            {
                                Name = "k"
                            }
                        }).Add(new ArgumentModel
                        {
                            Name = "addend",
                            Expression = new ExpressionModel
                            {
                                Name = "i"
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("i", "5"))
                    .Add(new RunArgModel("k", "3"))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);

            // assert
            Assert.AreEqual(0, result.Errors().Count);
            result.Match(answer =>
            {
                Assert.AreEqual("8", answer.Value);
                return 0;
            }
            , errors =>
            {
                Assert.Fail();
                return -1;
            });
        }
    }
}
