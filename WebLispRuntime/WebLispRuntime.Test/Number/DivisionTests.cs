﻿using ApiModel;
using Evaluation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace WebLispRuntime.Test.Number
{
    [TestClass]
    public class DivisionTests
    {
        [TestMethod]
        public void TestDivision()
        {
            // arrange
            ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("number"),
                    Body = new ExpressionModel
                    {
                        Name = "divide",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "dividend",
                            Expression = new ExpressionModel
                            {
                                Name = "k"
                            }
                        }).Add(new ArgumentModel
                        {
                            Name = "divisor",
                            Expression = new ExpressionModel
                            {
                                Name = "i"
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("i", "5"))
                    .Add(new RunArgModel("k", "20"))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);

            // assert
            Assert.AreEqual(0, result.Errors().Count);
            result.Match(answer =>
            {
                Assert.AreEqual("4", answer.Value);
                return 0;
            }
            , errors =>
            {
                Assert.Fail();
                return -1;
            });
        }
    }
}
