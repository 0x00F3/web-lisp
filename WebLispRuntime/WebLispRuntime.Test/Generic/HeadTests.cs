﻿using ApiModel;
using Evaluation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace WebLispRuntime.Test.Generic
{
    [TestClass]
    public class HeadTests
    {
        [TestMethod]
        public void HeadFail()
        {
            // arrange
            IImmutableList<decimal> initial = ImmutableList.Create<decimal>();

            ExecutionModel executable = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("number"),
                    Body = new ExpressionModel
                    {
                        Name = "head",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "operand",
                            Expression = new ExpressionModel
                            {
                                Name = "values"
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("values", initial.Select(i => new LiteralModel
                {
                    Value = i.ToString()
                })
                .ToImmutableList()))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(executable);

            // assert
            result.Match(answer =>
            {
                Assert.Fail("This should have failed because of the empty list.");
                return 0;
            },
            errors =>
            {
                Assert.AreEqual(1, errors.Count); //should be exactly one error
                return -1;
            });
        }
        [TestMethod]
        public void HeadPass()
        {
            // arrange
            IImmutableList<decimal> initial = ImmutableList.Create<decimal>(1)
                .Add(2)
                .Add(3)
                .Add(4);

            ExecutionModel executable = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("number"),
                    Body = new ExpressionModel
                    {
                        Name = "head",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "operand",
                            Expression = new ExpressionModel
                            {
                                Name = "values"
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("values", initial.Select(i => new LiteralModel
                {
                    Value = i.ToString()
                })
                .ToImmutableList()))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(executable);

            // assert
            result.Match(answer =>
            {
                Assert.AreEqual(answer.Value, "1");
                return 0;
            },
            errors =>
            {
                Assert.Fail();
                return -1;
            });
        }
    }
}
