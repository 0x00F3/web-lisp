﻿using ApiModel;
using Evaluation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace WebLispRuntime.Test.Generic
{
    [TestClass]
    public class ConditionalTests
    {
        [TestMethod]
        public void TestNumericConditional()
        {
            // arrange
            ExecutionModel executable = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("number"),
                    Body = new ExpressionModel
                    {
                        Name = "if",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "condition",
                            Expression = new ExpressionModel
                            {
                                Name = "cond"
                            }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "pass",
                            Expression = new ExpressionModel
                            {
                                Name = "a"
                            }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "fail",
                            Expression = new ExpressionModel
                            {
                                Name = "b"
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("cond", "false"))
                .Add(new RunArgModel("a", "8.4"))
                .Add(new RunArgModel("b", "6.5"))
            };

            // act
            Result<LiteralModel> result = 
                new ExecEvaluator().Execute(executable);

            // assert
            result.Match(answer =>
            {
                Assert.AreEqual("6.5", answer.Value);
                return 0;
            }
            , errors =>
            {
                Assert.Fail();
                return -1;
            });
        }
    }
}
