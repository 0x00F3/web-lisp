﻿using ApiModel;
using Evaluation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace WebLispRuntime.Test.Generic
{
    [TestClass]
    public class ReductionTests
    {
        [TestMethod]
        public void TestSum()
        {
            // arrange
            IImmutableList<decimal> initial = ImmutableList.Create<decimal>(1)
                .Add(2)
                .Add(3)
                .Add(4);

            ExecutionModel executable = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("number"),
                    Body = new ExpressionModel
                    {
                        Name = "reduce",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "operand",
                            Expression = new ExpressionModel
                            {
                                Name = "values"
                            }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "initial", // initial value of _accumulator
                            Expression = new ExpressionModel
                            {
                                Name = "0"
                            }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "f",
                            Expression = new ExpressionModel
                            {
                                Name = "lambda",
                                Arguments = ImmutableList.Create(new ArgumentModel
                                {
                                    Name = "expression",
                                    Expression = new ExpressionModel
                                    {
                                        Name = "add",
                                        Arguments = ImmutableList.Create(new ArgumentModel
                                        {
                                            Name = "augend",
                                            // _element and _accumulator are reserved
                                            // words in the instruction set.
                                            Expression = new ExpressionModel
                                            {
                                                Name = "myelement"
                                            }
                                        })
                                        .Add(new ArgumentModel
                                        {
                                            Name = "addend",
                                            Expression = new ExpressionModel
                                            {
                                                Name = "myaccumulator"
                                            }
                                        })
                                    }
                                }),
                                Types = ImmutableList.Create(new ParameterModel
                                {
                                    Name = "myaccumulator",
                                    Type = new TypeModel("number")
                                })
                                .Add(new ParameterModel
                                {
                                    Name = "myelement",
                                    Type = new TypeModel("number")
                                })
                                .Add(new ParameterModel
                                {
                                    Name = "TRETURN",
                                    Type = new TypeModel("number")
                                })
                            }
                        }),
                        Types = ImmutableList.Create(new ParameterModel
                        {
                            Index = 0,
                            Name = "T",
                            Type = new TypeModel { Name = "number" }
                        })
                        .Add(new ParameterModel
                        {
                            Index = 0,
                            Name = "TNEXT",
                            Type = new TypeModel { Name = "number" }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("values", initial.Select(i => new LiteralModel
                {
                    Value = i.ToString()
                })
                .ToImmutableList()))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(executable);

            // assert
            result.Match(answer =>
            {
                Assert.AreEqual(answer.Value, "10");
                return 0;
            },
            errors =>
            {
                Assert.Fail();
                return -1;
            });

        }
    }
}
