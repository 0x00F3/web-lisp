﻿using ApiModel;
using Evaluation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace WebLispRuntime.Test.List
{
    [TestClass]
    public class AdditionTests
    {
        [TestMethod]
        public void TestAppend()
        {
            IImmutableList<decimal> initial = ImmutableList.Create<decimal>(1)
                .Add(2)
                .Add(3);
            ExecutionModel model = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("list", new TypeModel("number")),
                    Body = new ExpressionModel
                    {
                        Name = "append",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "list",
                            Expression = new ExpressionModel
                            {
                                Name = "values"
                            }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "element",
                            Expression = new ExpressionModel
                            {
                                Name = "add",
                                Arguments = ImmutableList.Create(new ArgumentModel
                                {
                                    Name = "augend",
                                    // "_element" being a reserved word in the
                                    // instruction set because why not. 
                                    Expression = new ExpressionModel { Name = "3" }
                                }).Add(new ArgumentModel
                                {
                                    Name = "addend",
                                    Expression = new ExpressionModel
                                    {
                                        Name = "1"
                                    }
                                })
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("values", initial.Select(i => 
                new LiteralModel
                {
                    Value = i.ToString()
                })
                .ToImmutableList()))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(model);

            // assert
            result.Match(answer =>
            {
                //failures = all the expected answers not found in the actual answer
                IImmutableList<LiteralModel> failures = initial.Add(4)
                    .Select(i => new LiteralModel(i.ToString()))
                    .Where(expected => answer.Values.Where(ans => ans.Value == expected.Value).Count() != 1)
                    .ToImmutableList();
                Assert.AreEqual(0, failures.Count());
                Assert.AreEqual(initial.Count() + 1, answer.Values.Count());
                return 0;
            },
            errors =>
            {
                Assert.Fail();
                return -1;
            });
        }
    }
}
