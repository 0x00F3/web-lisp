﻿using ApiModel;
using Evaluation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace WebLispRuntime.Test.List
{
    [TestClass]
    public class ProjectionTests
    {
        

        [TestMethod]
        public void TestIntToBool()
        {
            IImmutableList<LiteralModel> xs =
                ImmutableList.Create(new LiteralModel
                {
                    Value = "-1"
                })
                .Add(new LiteralModel
                {
                    Value = "0"
                })
                .Add(new LiteralModel
                {
                    Value = "1"
                });

            ExpressionModel model = new ExpressionModel
            {
                Name = "map",
                Arguments = ImmutableList.Create(new ArgumentModel
                {
                    Name = "operand",
                    Expression = new ExpressionModel
                    {
                        Name = "xs"
                    }
                })
                .Add(new ArgumentModel
                {
                    Name = "f",
                    Expression = new ExpressionModel
                    {
                        Name = "lambda",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "expression",
                            Expression = new ExpressionModel
                            {
                                Name = "greater-than",
                                Arguments = ImmutableList.Create(new ArgumentModel
                                {
                                    Name = "first",
                                    Expression = new ExpressionModel { Name = "x" }
                                })
                                .Add(new ArgumentModel
                                {
                                    Name = "second",
                                    Expression = new ExpressionModel { Name = "0" }
                                })
                            }
                        }),
                        Types = ImmutableList.Create(new ParameterModel
                        {
                            Name = "x",
                            Index = 0,
                            Type = new TypeModel { Name = "number" }
                        })
                        .Add(new ParameterModel
                        {
                            Name = "TRETURN",
                            Index = 1,
                            Type = new TypeModel { Name = "boolean" }

                        })
                    }
                }),
                Types = ImmutableList.Create(new ParameterModel
                {
                    Index = 0,
                    Name = "T",
                    Type = new TypeModel { Name = "number" }
                })
                .Add(new ParameterModel
                {
                    Index = 0,
                    Name = "TNEXT",
                    Type = new TypeModel { Name = "boolean" }
                })
            };

            ExecutionModel sut = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Body = model,
                    Type = new TypeModel("list", new TypeModel("boolean"))
                },
                RunArgs = ImmutableList.Create(new RunArgModel
                {
                    Name = "xs",
                    Literal = new LiteralModel("list", xs)
                })
            };

            Result<LiteralModel> result = new ExecEvaluator().Execute(sut);

            // assert
            result.Match(answer =>
            {
                Assert.AreEqual(3, answer.Values.Count);
                Assert.AreEqual("False", answer.Values.ElementAt(0).Value);
                Assert.AreEqual("False", answer.Values.ElementAt(1).Value);
                Assert.AreEqual("True", answer.Values.ElementAt(2).Value);
                return 0;
            },
            errors =>
            {
                Assert.Fail();
                return -1;
            });

        } // end method
    } // end class
} // end namespace
