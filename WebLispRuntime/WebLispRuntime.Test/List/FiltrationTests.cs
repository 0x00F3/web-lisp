﻿using ApiModel;
using Evaluation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace WebLispRuntime.Test.List
{
    [TestClass]
    public class FiltrationTests
    {
        [TestMethod]
        public void TestFilter()
        {
            // arrange
            IImmutableList<decimal> initial = ImmutableList.Create<decimal>(1)
                .Add(2)
                .Add(3)
                .Add(4);
            ExecutionModel executable = new ExecutionModel
            {
                Dfn = new DefinitionModel
                {
                    Type = new TypeModel("list", new TypeModel("number")),
                    Body = new ExpressionModel
                    {
                        Name = "filter",
                        Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "operand",
                            Expression = new ExpressionModel
                            {
                                Name = "values"
                            }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "f",
                            Expression = new ExpressionModel
                            {
                                Name = "lambda",
                                Arguments = ImmutableList.Create(new ArgumentModel
                                {
                                    Name = "expression",
                                    Expression = new ExpressionModel
                                    {
                                        Name = "greater-than",
                                        Arguments = ImmutableList.Create(new ArgumentModel
                                        {
                                            Name = "first",
                                            // "_element" being a reserved word in the
                                            // instruction set because why not. 
                                            Expression = new ExpressionModel
                                            {
                                                Name = "e"
                                            }
                                        }).Add(new ArgumentModel
                                        {
                                            Name = "second",
                                            Expression = new ExpressionModel { Name = "2" }
                                        })
                                    }
                                }),
                                Types = ImmutableList.Create(new ParameterModel
                                {
                                    Name = "e",
                                    Type = new TypeModel("number")
                                })
                                .Add(new ParameterModel
                                {
                                    Name = "TRETURN",
                                    Type = new TypeModel("boolean")
                                })
                            }
                        })
                    }
                },
                RunArgs = ImmutableList.Create(new RunArgModel("values", initial.Select(i => new LiteralModel
                {
                    Value = i.ToString()
                })
                .ToImmutableList()))
            };

            // act
            Result<LiteralModel> result = new ExecEvaluator().Execute(executable);

            // assert
            result.Match(answer =>
            {
                IImmutableList<decimal> expected = initial.Where(e => e > 2)
                    .ToImmutableList();
                //failures = all the expected answers not found in the actual answer
                IImmutableList<LiteralModel> failures = expected
                    .Select(i => new LiteralModel(i.ToString()))
                    .Where(ex => answer.Values.Where(ans => ans.Value == ex.Value).Count() != 1)
                    .ToImmutableList();
                Assert.AreEqual(0, failures.Count());
                Assert.AreEqual(expected.Count(), answer.Values.Count());
                return 0;
            },
            errors =>
            {
                Assert.Fail();
                return -1;
            });
        }
    }
}
