﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    /// <summary>
    /// a string extension methods class for parsing strings into Maybes.
    /// </summary>
    public static class ParseExtensions
    {
        public static Maybe<bool> ParseBool(this string s)
           => bool.TryParse(s, out bool result)
               ? new Maybe<bool>(result)
               : new Maybe<bool>();

        public static Maybe<DateTime> ParseDate(this string iso8601)
            => DateTime.TryParse(iso8601, out DateTime result)
                ? new Maybe<DateTime>(result)
                : new Maybe<DateTime>();

        public static Maybe<decimal> ParseDecimal(this string s)
            => decimal.TryParse(s, out decimal result)
                ? new Maybe<decimal>(result)
                : new Maybe<decimal>();

       
    }
}
