﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Core
{
    public static class EnumerableExtensions
    {
        public static Maybe<T> MaybeElementAt<T>(this IEnumerable<T> xs, int i)
        {
            try { return new Maybe<T>(xs.ElementAt(i)); }
            catch { return new Maybe<T>(); }
        }

        /// <summary>
        /// if the IEnumerable has elements, it returns a full maybe, else it
        /// returns an empty maybe.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xs"></param>
        /// <returns></returns>
        public static Maybe<T> MaybeFirst<T>(this IEnumerable<T> xs)
            => new Maybe<T>(xs.FirstOrDefault());

        /// <summary>
        /// if the IEnumerable has elements, it returns a full maybe, else it
        /// returns an empty maybe.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xs"></param>
        /// <returns></returns>
        public static Maybe<T> MaybeLast<T>(this IEnumerable<T> xs)
            => new Maybe<T>(xs.LastOrDefault());

        /// <summary>
        /// given an IEnumerable of maybes, returns an immutable list of the 
        /// contents of only the full maybes. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="maybes"></param>
        /// <returns></returns>
        public static IImmutableList<T> ToFlatList<T>(this IEnumerable<Maybe<T>> maybes)
        {
            IImmutableList<T> result = ImmutableList.Create<T>();
            foreach (Maybe<T> element in maybes)
            {
                element.Map(e => result = result.Add(e));
            }
            return result;
        }

    }
}
