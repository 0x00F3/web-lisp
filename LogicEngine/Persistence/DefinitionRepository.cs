﻿using Core;
using AbstractSyntaxTree;
using AbstractSyntaxTree.Bool;
using AbstractSyntaxTree.Decimal;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Persistence
{
    public class DefinitionRepository
    {
        private IImmutableList<IDefinition> builtInOps
            = ImmutableList.Create<IDefinition>(new Inversion())
                .Add(new Addition());

        public bool NameExists(string name)
            => builtInOps.Where(dfn => dfn.Name() == name).Any();

        public Maybe<IDefinition> GetByName(string name)
            => new Maybe<IDefinition>(builtInOps.Where(dfn => dfn.Name() == name).FirstOrDefault());
    }
}
