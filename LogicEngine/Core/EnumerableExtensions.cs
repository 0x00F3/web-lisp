﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    public static class EnumerableExtensions
    {
        public static Maybe<T> MaybeFirst<T>(this IEnumerable<T> xs)
            => new Maybe<T>(xs.FirstOrDefault());
    }
}
