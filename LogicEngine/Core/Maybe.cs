﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public class Maybe<T>
    {
        private readonly T value;
        
        public Maybe() { }
        
        public Maybe(T value)
        {
            this.value = value;
        }

        /// <summary>
        /// executes some on the Maybe if there is a value on which to execute,
        /// else returns an empty Maybe.
        /// </summary>
        /// <typeparam name="TNext"></typeparam>
        /// <param name="some"></param>
        /// <returns></returns>
        public Maybe<TNext> Bind<TNext>(Func<T, TNext> some)
            => value == null 
            ? new Maybe<TNext>() 
            : new Maybe<TNext>(some(value));

        /// <summary>
        /// bind with a function that returns a Maybe to avoid nested Maybes.
        /// </summary>
        /// <typeparam name="TNext"></typeparam>
        /// <param name="some"></param>
        /// <returns></returns>
        public Maybe<TNext> FlatMap<TNext>(Func<T, Maybe<TNext>> some)
            => Match(val => some(val), () => new Maybe<TNext>());

        /// <summary>
        /// takes two functions to handle both cases of Maybe and returns the
        /// result of the appropriate one. 
        /// </summary>
        /// <typeparam name="TNext"></typeparam>
        /// <param name="some"></param>
        /// <param name="none"></param>
        /// <returns></returns>
        public TNext Match<TNext>(Func<T, TNext> some, Func<TNext> none)
            => value == null ? none() : some(value);

    }
}
