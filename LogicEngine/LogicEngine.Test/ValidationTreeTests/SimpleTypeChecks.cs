﻿using ApiModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;
using ValidationTree;

namespace LogicEngine.Test.ValidationTreeTests
{
    //this class name is terrible, but it's fine for now.
    [TestClass]
    public class SimpleTypeChecks
    {
        [TestMethod]
        public void TestBooleanFailure()
        {
            // arrange
            DefinitionModel sut = new DefinitionModel
            {
                Name = "not-wrapper",
                Parameters = ImmutableList.Create(new ParameterModel
                    { Name = "c", Type = "number" }
                ),
                Type = "number",
                Body = new ExpressionModel
                {
                    Name = "not",
                    Arguments = ImmutableList.Create(new ArgumentModel
                    {
                        Name = "operand",
                        Expression = new ExpressionModel { Value = "c" }
                    })
                }
            };

            // act
            var result = new DefinitionValidator().Validate(sut);

            // assert
            /* that there are one or more type errors. One should assume that 
             * there is no way to predict how many type errors there will be
             * if there are type errors, and it is expected that the number of
             * errors returned for a given invalid definition will change. 
             * These changes should not be considered breaking.*/
            Assert.AreNotEqual(0, result.Count);
        }

        [TestMethod]
        public void TestBooleanPass()
        {
            // arrange
            DefinitionModel sut = new DefinitionModel
            {
                Name = "not-wrapper",
                Parameters = ImmutableList.Create(new ParameterModel
                    { Name = "c", Type = "boolean" }
                ),
                Type = "boolean",
                Body = new ExpressionModel
                {
                    Name = "not",
                    Arguments = ImmutableList.Create(new ArgumentModel
                    {
                        Name = "operand",
                        Expression = new ExpressionModel { Value = "c" }
                    })
                }
            };

            // act
            var result = new DefinitionValidator().Validate(sut);

            // assert
            Assert.AreEqual(0, result.Count);
        }

    }
}
