﻿using ApiModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace LogicEngine.Test.ApiModelTests
{
    [TestClass]
    class DefinitionModelTest
    {
        [TestMethod]
        void TestDefinitionConstruction()
        {
            DefinitionModel sut = new DefinitionModel
            {
                Name = "example-function",
                Parameters = ImmutableList.Create<ParameterModel>()
                    .Add(new ParameterModel { Name = "a", Type = "boolean" })
                    .Add(new ParameterModel { Name = "b", Type = "number" })
                    .Add(new ParameterModel { Name = "c", Type = "number" }),
                Type = "number",
                Body = new ExpressionModel
                {
                    Name = "if",
                    Arguments = ImmutableList.Create(new ArgumentModel
                        {
                            Name = "condition",
                            Expression = new ExpressionModel { Value = "a" }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "pass",
                            Expression = new ExpressionModel
                            {
                                Name = "add",
                                Arguments = ImmutableList.Create(new ArgumentModel
                                    {
                                        Name = "augend",
                                        Expression = new ExpressionModel { Value = "b" }
                                    })
                                    .Add(new ArgumentModel
                                    {
                                        Name = "added",
                                        Expression = new ExpressionModel { Value = "c" }
                                    })
                            }
                        })
                        .Add(new ArgumentModel
                        {
                            Name = "fail",
                            Expression = new ExpressionModel
                            {
                                Name = "subtract",
                                Arguments = ImmutableList.Create(new ArgumentModel
                                    {
                                        Name = "minuend",
                                        Expression = new ExpressionModel { Value = "b" }
                                    })
                                    .Add(new ArgumentModel
                                    {
                                        Name = "subtrahend",
                                        Expression = new ExpressionModel { Value = "c" }
                                    })
                            }
                        })
                }
            };
        }
    }
}
