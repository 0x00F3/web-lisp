﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace AbstractSyntaxTree.Decimal
{
    public class Subtraction : IDefinition
    {
        string IDefinition.Name() => "subtract";

        IImmutableList<Parameter> IDefinition.Parameters()
            => ImmutableList.Create(new Parameter("minuend", new Number()))
                .Add(new Parameter("subtrahend", new Number()));

        IType IDefinition.Type()
        {
            throw new NotImplementedException();
        }
    }
}
