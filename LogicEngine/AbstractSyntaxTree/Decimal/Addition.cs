﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace AbstractSyntaxTree.Decimal
{
    public class Addition : IDefinition
    {
        string IDefinition.Name() => "add";

        IImmutableList<Parameter> IDefinition.Parameters()
            => ImmutableList.Create(new Parameter("augend", new Number()))
                .Add(new Parameter("addend", new Number()));

        IType IDefinition.Type() => new Number();
    }
}
