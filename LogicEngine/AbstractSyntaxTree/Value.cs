﻿using ApiModel;
using Core;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace AbstractSyntaxTree
{
    public class Value : IDefinition
    {
        //TODO what a mess.
        private Value(string value) { }
        string IDefinition.Name() => "value";

        IImmutableList<Parameter> IDefinition.Parameters()
            => ImmutableList.Create(new Parameter("value", new Number()));

        IType IDefinition.Type()
        {
            throw new NotImplementedException();
        }

        
    }
}
