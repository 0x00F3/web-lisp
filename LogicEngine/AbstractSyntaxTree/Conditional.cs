﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace AbstractSyntaxTree
{
    /// <summary>
    /// the "if" operator
    /// </summary>
    public class Conditional : IDefinition
    {
        /* Important to keep in mind: I can derive the type of the conditional
         * from how it's used in the outside context--I don't have to figure it
         * out by the type of the pass/fail arguments. This should be fine. */
        private readonly IType type;

        public Conditional(IType type)
        {
            this.type = type ?? throw new ArgumentNullException(nameof(type));
        }

        string IDefinition.Name() => "if";

        IImmutableList<Parameter> IDefinition.Parameters()
            => ImmutableList.Create(new Parameter("condition", new Number()))
                .Add(new Parameter("pass", type))
                .Add(new Parameter("fail", type));

        IType IDefinition.Type() => type;
    }
}
