﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace AbstractSyntaxTree
{
    public interface IDefinition
    {
        string Name();
        IImmutableList<Parameter> Parameters();
        IType Type();
    }
}
