﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractSyntaxTree
{
    public class Argument
    {
        private readonly string name;

        /* one or the other of the following two values will always be null. 
         * Accessor methods will be very important to prevent NullReffrence.*/
        private readonly string leaf;
        private readonly IDefinition node;

    }
}
