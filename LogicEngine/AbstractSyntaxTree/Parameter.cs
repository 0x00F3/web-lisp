﻿using ApiModel;
using Core;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace AbstractSyntaxTree
{
    public class Parameter
    {
        private readonly string name;
        private readonly IType type;

        internal Parameter(string name, IType type)
        {
            this.name = name ?? throw new ArgumentNullException(nameof(name));
            this.type = type ?? throw new ArgumentNullException(nameof(type));
        }

        public string Name() => name;
        public IType Type() => type;

        public static Result<Parameter> Validate(ParameterModel model)
            => checkType(model)
                .AddErrors(checkName(model.Name))
                .Map(type => new Parameter(model.Name, type))
                ;

        public static Result<IType> checkType(ParameterModel model)
            => TypeChecker.AsType(model.Type)
                .Match(type => new Result<IType>(type)
                , () => new Result<IType>
                    (ImmutableList.Create(invalidParameterTypeError(model)))
                )
                ;

        /// <summary>
        /// validates parameter name (currently defined as just not being a 
        /// literal).
        /// </summary>
        /// <param name="name"></param>
        /// <returns>an error message if applicable.</returns>
        private static IImmutableList<string> checkName(string name)
            => TypeChecker.IsLiteral(name)
            ? ImmutableList.Create(invalidParameterName(name))
            : ImmutableList.Create<string>();

        /*----------------------------error messages-------------------------*/
        private static string invalidParameterName(string invalidName)
            => $"The parameter {invalidName} has an invalid identifier. Please ensure parameters are not called 'true', 'false' or any number.";

        private static string invalidParameterTypeError(ParameterModel model)
            => $"The parameter {model.Name} has an invalid type {model.Type}. Please make sure all parameters are are either booleans or numbers.";

    }
}
