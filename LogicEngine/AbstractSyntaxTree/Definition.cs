﻿using ApiModel;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace AbstractSyntaxTree
{
    public class Definition : IDefinition
    {
        private readonly string name;
        private readonly IDefinition head;
        private readonly IImmutableList<Argument> args;
        private readonly IImmutableList<Parameter> parameters;
        private readonly IType type;

        string IDefinition.Name() => name;

        IImmutableList<Parameter> IDefinition.Parameters() => parameters;

        IType IDefinition.Type() => type;
        
    }
}
