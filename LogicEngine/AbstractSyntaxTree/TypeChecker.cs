﻿using Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractSyntaxTree
{
    public static class TypeChecker
    {
        public static Maybe<IType> AsType(string type)
            => type == "boolean" ? new Maybe<IType>(new Boolean())
                : type == "number" ? new Maybe<IType>(new Number())
                : new Maybe<IType>();

        /// <summary>
        /// returns true if the type given is supported as a type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsType(string type) 
            => type == "boolean" | type == "number";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns>the IType if it can be evaluated as a boolean or
        /// number
        /// </returns>
        public static Maybe<IType> InferType(string value)
            => value == "true" | value == "false"
            ? new Maybe<IType>(new Boolean())
            : decimal.TryParse(value, out decimal result)
            ? new Maybe<IType>(new Number())
            : new Maybe<IType>();

        public static bool IsLiteral(string value)
            => InferType(value).Match(t => true, () => false);

    }
}
