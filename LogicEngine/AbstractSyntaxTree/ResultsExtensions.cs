﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace AbstractSyntaxTree
{
    public static class ResultsExtensions
    {
        public static IImmutableList<string> AllErrors<T>(IImmutableList<Result<T>> results)
            => results.SelectMany(result => result.Match(r => ImmutableList.Create<string>()
                                , es => es))
            .ToImmutableList();

        public static IImmutableList<T> AllValid<T>(IImmutableList<Result<T>> results)
            => results.SelectMany(result => result.Match(r => ImmutableList.Create(r), es => ImmutableList.Create<T>()))
            .ToImmutableList();

        public static Result<IImmutableList<T>> Invert<T>(this IImmutableList<Result<T>> originals)
            => InvertHelper(AllValid(originals), AllErrors(originals));

        public static Result<IImmutableList<T>> InvertHelper<T>(IImmutableList<T> valids, IImmutableList<string> errors)
            => errors.Any()
            ? new Result<IImmutableList<T>>(errors)
            : new Result<IImmutableList<T>>(valids);
    }
}
