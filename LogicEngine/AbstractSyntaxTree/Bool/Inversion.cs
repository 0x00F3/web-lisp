﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace AbstractSyntaxTree.Bool
{
    public class Inversion : IDefinition
    {
        string IDefinition.Name() => "not";

        IImmutableList<Parameter> IDefinition.Parameters()
            => ImmutableList.Create(new Parameter("operand", new Boolean()));

        IType IDefinition.Type() => new Boolean();
    }
}
