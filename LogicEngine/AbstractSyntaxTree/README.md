﻿# Read me

This library defines the conversion from API models to IOperations.
Importantly, invalid states are representable because types are represented as
strings.
