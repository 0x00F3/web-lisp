﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractSyntaxTree
{
    class Boolean : IType
    {
        string IType.Name() => "boolean";
    }
}
