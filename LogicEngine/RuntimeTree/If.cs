﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RuntimeTree
{
    class If<T> : IOperation<T>
    {
        private readonly IOperation<bool> condition;
        private readonly IOperation<T> pass;
        private readonly IOperation<T> fail;

        public If(IOperation<bool> condition, IOperation<T> pass, IOperation<T> fail)
        {
            this.condition = condition ?? throw new ArgumentNullException(nameof(condition));
            this.pass = pass ?? throw new ArgumentNullException(nameof(pass));
            this.fail = fail ?? throw new ArgumentNullException(nameof(fail));
        }

        T IOperation<T>.Exec() => condition.Exec() ? pass.Exec() : fail.Exec();
        
    }
}
