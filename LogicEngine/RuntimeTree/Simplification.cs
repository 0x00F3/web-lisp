﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RuntimeTree
{
    class Simplification<T> : IOperation<T>
    {
        private readonly T value;

        public Simplification(T value)
        {
            this.value = value;
        }

        T IOperation<T>.Exec() => value;
        
    }
}
