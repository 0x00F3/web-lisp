﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RuntimeTree.Bool
{
    class Not : IOperation<bool>
    {
        private readonly IOperation<bool> operand;

        public Not(IOperation<bool> operand)
        {
            this.operand = operand ?? throw new ArgumentNullException(nameof(operand));
        }

        bool IOperation<bool>.Exec() => !operand.Exec();
        
    }
}
