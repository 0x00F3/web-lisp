﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RuntimeTree.Decimal
{
    class Multiply : IOperation<decimal>
    {
        private readonly IOperation<decimal> multiplicand;
        private readonly IOperation<decimal> multiplier;

        public Multiply(IOperation<decimal> multiplicand, IOperation<decimal> multiplier)
        {
            this.multiplicand = multiplicand ?? throw new ArgumentNullException(nameof(multiplicand));
            this.multiplier = multiplier ?? throw new ArgumentNullException(nameof(multiplier));
        }

        decimal IOperation<decimal>.Exec() => multiplicand.Exec() * multiplicand.Exec();
        
    }
}
