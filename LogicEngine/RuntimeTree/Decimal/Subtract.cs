﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RuntimeTree.Decimal
{
    class Subtract : IOperation<decimal>
    {
        private readonly IOperation<decimal> minuend;
        private readonly IOperation<decimal> subtrahend;

        public Subtract(IOperation<decimal> minuend, IOperation<decimal> subtrahend)
        {
            this.minuend = minuend ?? throw new ArgumentNullException(nameof(minuend));
            this.subtrahend = subtrahend ?? throw new ArgumentNullException(nameof(subtrahend));
        }

        decimal IOperation<decimal>.Exec() => minuend.Exec() - subtrahend.Exec();
    }
}
