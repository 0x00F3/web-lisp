﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RuntimeTree.Decimal
{
    class Divide : IOperation<decimal>
    {
        private readonly IOperation<decimal> dividend;
        private readonly IOperation<decimal> divisor;

        public Divide(IOperation<decimal> dividend, IOperation<decimal> divisor)
        {
            this.dividend = dividend ?? throw new ArgumentNullException(nameof(dividend));
            this.divisor = divisor ?? throw new ArgumentNullException(nameof(divisor));
        }

        decimal IOperation<decimal>.Exec() => dividend.Exec() / divisor.Exec();
        
    }
}
