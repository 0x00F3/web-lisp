﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RuntimeTree.Decimal
{
    class Add : IOperation<decimal>
    {
        private readonly IOperation<decimal> augend;
        private readonly IOperation<decimal> addend;

        public Add(IOperation<decimal> augend, IOperation<decimal> addend)
        {
            this.augend = augend ?? throw new ArgumentNullException(nameof(augend));
            this.addend = addend ?? throw new ArgumentNullException(nameof(addend));
        }

        decimal IOperation<decimal>.Exec() => augend.Exec() + addend.Exec();
        
    }
}
