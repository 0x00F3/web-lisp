﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RuntimeTree
{
    class UserDefined<T> : IOperation<T>
    {
        private readonly string name;
        private readonly IOperation<T> head;

        public UserDefined(string name, IOperation<T> head)
        {
            this.name = name ?? throw new ArgumentNullException(nameof(name));
            this.head = head ?? throw new ArgumentNullException(nameof(head));
        }

        T IOperation<T>.Exec() => head.Exec();
        
    }
}
