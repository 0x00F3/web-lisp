﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RuntimeTree
{
    public interface IOperation<T>
    {
        T Exec();
    }
}
