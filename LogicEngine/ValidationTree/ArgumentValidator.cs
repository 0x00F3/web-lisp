﻿using Core;
using ApiModel;
using AbstractSyntaxTree;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace ValidationTree
{
    public class ArgumentValidator
    {
        /// <summary>
        /// compares the definition from Persistence with the way it is used in
        /// the new expression from the user. 
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <returns>an error list of ways actual does not match expected.
        /// </returns>
        public IImmutableList<string> Validate(IDefinition expected
            , IImmutableList<ArgumentModel> actual
            , IImmutableList<Parameter> context)
            => validateLength(expected.Name(), expected.Parameters().Count, actual.Count)
                .AddRange(matchAll(expected.Parameters(), actual, context))
                ;

        /* ## Argument / Parameter Comparison
         * 1) the argument list should be as long as the parameter list
         * 2) for each parameter, there should exist an argument that
         *      a) has the same name
         *      b) has the same type
         * 3) the argument should validate as an expression.
         */

        /*---------------------individual validation rules-------------------*/
        
            
        private IImmutableList<string> validateLength(string name, int expected, int actual)
            => expected == actual
            ? ImmutableList.Create<string>()
            : ImmutableList.Create(argumentLengthError(name, expected, actual))
            ;

        private IImmutableList<string> match(Parameter target
            , IImmutableList<ArgumentModel> range
            , IImmutableList<Parameter> context)
            => range.Where(arg => arg.Name == target.Name())
                .MaybeFirst()
                .Match(arg => new ExpressionValidator().Validate(target.Type(), arg.Expression, context)
                , () => ImmutableList.Create("No such argument.")
                ); //todo: better error name

        private IImmutableList<string> matchAll(IImmutableList<Parameter> expected
            , IImmutableList<ArgumentModel> actual
            , IImmutableList<Parameter> context)
            => expected.SelectMany(p => match(p, actual, context)).ToImmutableList();
        /*---------------------------error messages--------------------------*/
        private string argumentLengthError(string name, int expected, int actual)
            => $"The expression {name} expects {expected} arguments, but {actual} were supplied.";
    }
}
