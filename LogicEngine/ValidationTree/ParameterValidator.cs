﻿using ApiModel;
using AbstractSyntaxTree;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace ValidationTree
{
    internal class ParameterValidator
    {

        internal Result<IImmutableList<Parameter>> Validate(IImmutableList<ParameterModel> parameters)
            => parameters.Select(p => Parameter.Validate(p))
                .ToImmutableList()
                .Invert()
                .AddErrors(uniqueParameters(parameters));

        /*----------------------------------rule validators------------------*/

        private IImmutableList<string> uniqueParameters(IImmutableList<ParameterModel> parameters)
            => parameters.Select(p => p.Name).Distinct().Count() == parameters.Count()
            ? ImmutableList.Create<string>()
            : ImmutableList.Create(duplicateParameterError());


        /*----------------------------error messages-------------------------*/
        private string duplicateParameterError()
            => "There are duplicates in the list of parameter names for this definition. Please make sure parameter names are unique.";

    }
}
