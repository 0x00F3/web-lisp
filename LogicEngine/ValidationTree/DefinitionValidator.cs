﻿using ApiModel;
using AbstractSyntaxTree;
using Persistence;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace ValidationTree
{
    public class DefinitionValidator
    {
        /* A definition is valid iff */
        public IImmutableList<string> Validate(DefinitionModel model)
            // 1) the identifier is unique
            => isUsedAlready(model.Name)
            // 2) the parameters have valid types
                .AddRange(new ParameterValidator().Validate(model.Parameters).Errors())
            // 3) the body type matches the declared type
            // 4) the body type checks as an expression, based on the 
            //      parameters in the definiton context.
                .AddRange(validateBody(model))
                ;

        /*-------------------individual validators---------------------------*/
        private IImmutableList<string> isUsedAlready(string definitionName)
            => new DefinitionRepository().NameExists(definitionName)
            ? ImmutableList.Create(definitionNameError(definitionName))
            : ImmutableList.Create<string>();

        private IImmutableList<string> validateBody(DefinitionModel model)
            => TypeChecker.AsType(model.Type)
                .Match(type => new ExpressionValidator()
                                .Validate(type, model.Body, model.Parameters)
                , () => ImmutableList.Create(definitionTypeError(model.Type))
                );

        private Result<IType> validateType(string typeName)
            => TypeChecker.AsType(typeName)
                .Match(type => new Result<IType>(type),
                    () => new Result<IType>(ImmutableList.Create(definitionTypeError(typeName)))
                );

        /*--------------------------error messages---------------------------*/
        private string definitionNameError(string name)
            => $"The rule name \"{name}\" has been used already. Please choose a new, unique identifier.";

        private string definitionTypeError(string type)
            => $"For this definition, the given type \"{type}\" is not recognized. Please choose either \"number\" or \"boolean\"";

    }
}
