﻿using ApiModel;
using AbstractSyntaxTree;
using Persistence;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace ValidationTree
{
    public class ExpressionValidator
    {
        public IImmutableList<string> Validate(IType expected
            , ExpressionModel actual
            , IImmutableList<Parameter> context
        ) => actual.Either()
                .Match((args) => Validate(expected, actual)
                , val => new ValueValidator().Validate(expected, val, context)
                );
        
        /* An expression is valid iff. */
        // 1) the definition for the expression exists in the database
        // 2) the type expected by the context matches the defined type in
        //      Persistence.
        // 3) each argument type checks with its expectation from the db. 

        public IImmutableList<string> Validate(IType expected, ExpressionModel actual)
            => new DefinitionRepository().GetByName(actual.Name)
                .Match(dfn => compare(expected, dfn) //and... TODO
                , () => ImmutableList.Create(definitionNotFound(actual.Name))
                );

        /*---------------------------helper methods--------------------------*/
        /// <summary>
        /// checks the type of the fetched definition against the type expected
        /// by the current context of the model.
        /// </summary>
        /// <param name="expected">the type from the current validation
        /// context
        /// </param>
        /// <param name="actual">the type from persistence.</param>
        /// <returns></returns>
        private IImmutableList<string> compare(IType expected, IDefinition actual)
            => expected.Name() == actual.Type().Name()
            ? ImmutableList.Create<string>()
            : ImmutableList.Create(typeError(expected, actual));
        
        /*--------------------------error messages---------------------------*/
        private string definitionNotFound(string name)
            => $"The definition for {name} could not be found. Please make sure you have defined all necessary rules.";

        private string typeError(IType expected, IDefinition fromPeristence)
            => $"The expression \"{fromPeristence.Name()}\" has type {fromPeristence.Type().Name()} but is used like a {expected.Name()}. Please make sure types match.";
    }
}
