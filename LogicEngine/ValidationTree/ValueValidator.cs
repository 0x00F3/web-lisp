﻿using ApiModel;
using Core;
using AbstractSyntaxTree;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace ValidationTree
{
    public class ValueValidator
    {
        /* values are valid if the type is right for the current
         * context */
        /* get the parameter associated referenced by the value and then compare*/

        public IImmutableList<string> Validate(IType expected, string actual, IImmutableList<Parameter> context)
            => inferType(actual, context)
                .Match(type => compare(expected, type, actual)
                , () => ImmutableList.Create($"Unable to infer type of {actual}")
                );

        private IImmutableList<string> compare(IType expected, IType actual, string valueName)
            => expected.Name() == actual.Name()
            ? ImmutableList.Create<string>()
            : ImmutableList.Create(valueTypeError(valueName, actual.Name(), expected.Name()));

        /// <summary>
        /// evaluates a value to see if it is a number, boolean, or parameter.
        /// If it is a parameter, it returns the type of the parameter, else
        /// it returns the inferred type (number or boolean).
        /// 
        /// If it cannot infer the type, it fails with an empty Maybe.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private Maybe<IType> inferType(string actual, IImmutableList<Parameter> parameters)
            => TypeChecker.InferType(actual)
                .Match(t => new Maybe<IType>(t), 
                    () => getTypeOfParameter(parameters, actual)
                );

        private Maybe<IType> getTypeOfParameter(IImmutableList<Parameter> ps, string name)
            => ps.Where(p => p.Name() == name)
                .MaybeFirst()
                .Bind(p => p.Type());

        private string valueTypeError(string valueName, string actual, string expected)
            => $"The value {valueName} has an inferred type of {actual} where {expected} is expected. Please revise.";
    }
}
