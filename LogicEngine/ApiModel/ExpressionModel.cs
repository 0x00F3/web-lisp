﻿using Core;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace ApiModel
{
    /* Okay, this is a work in progress (2019-11-13). Here's what I'm going for
     * now:
     *  * The Arguments and Value fields are mutually exclusive. That is, in a 
     *      valid model, one or the other of them should be null. I can't
     *      enforce this because the ASP.NET MVC model binder is just not that
     *      smart. I will be relying on smart usage somewhat, unfortunately. 
     *  * We can limit the complexity that this introduces by referencing
     *      only the Either(). When you need to interact with either the
     *      Arguments or the Value, assume you could have either one and pass
     *      in functions to satisfy both possible cases. 
     *  
     *  Something to think about: A custom model binder might solve some of 
     *  these woes. Will consider. 
     *  --eleanor
     */
    public class ExpressionModel 
    {
        public string Name { get; set; }

        /// <summary>
        /// THIS IS ONLY PUBLIC SO THE MODEL BINDER CAN USE IT. DO NOT 
        /// REFERENCE. (except in the test suite). USE THE Either!
        /// </summary>
        public IImmutableList<ArgumentModel> Arguments { get; set; }

        /// <summary>
        /// THIS IS ONLY PUBLIC SO THE MODEL BINDER CAN USE IT. DO NOT 
        /// REFERENCE. (except in the test suite). USE THE Either!
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// This does not validate the object as having ONLY one or the other.
        /// Please validate separately. 😞
        /// </summary>
        /// <returns></returns>
        public Either<IImmutableList<ArgumentModel>, string> Either()
            => Arguments == null
            ? new Either<IImmutableList<ArgumentModel>, string>(Value)
            : new Either<IImmutableList<ArgumentModel>, string>(Arguments);
    }
}
