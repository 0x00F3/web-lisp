﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiModel
{
    public class ParameterModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
