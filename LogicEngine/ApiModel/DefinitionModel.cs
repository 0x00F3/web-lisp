﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace ApiModel
{
    public class DefinitionModel
    {
        public string Name { get; set; }
        public IImmutableList<ParameterModel> Parameters {get; set; }
        public string Type { get; set; }
        public ExpressionModel Body { get; set; }
    }
}
