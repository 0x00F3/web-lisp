﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiModel
{
    public class ArgumentModel
    {
        public string Name { get; set; }
        public ExpressionModel Expression { get; set; }

    }
}
